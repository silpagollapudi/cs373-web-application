# CS373 Web Application

CS373 web application

Name: Danyaal Ali
EID: da27794
GitlabID: @Danyaalali 

Name: Sanjna Sunil
EID: ss75769
GitlabID: @sanjnasunil

Name: Silpa Gollapudi
EID: sg43632
GitlabID: @silpagollapudi 

Name: Eduardo Pineda
EID: ep23934
GitlabID: @edibertusmax

Name: Brandon Manibo
EID: bam4435
GitlabID: @brandonmanibo 

Git SHA: a7486ede4602d7cb534e5bb11ed970ffa2f0a346

Gitlab pipeline: https://gitlab.com/silpagollapudi/cs373-web-application/pipelines

Link to website: http://disasterfund.me/

Estimated completion time for each member (hours: int)
Danyaal Ali: 7
Sanjna Sunil: 3
Silpa Gollapudi: 3
Eduardo Pineda: 8
Brandon Manibo: 10

Actual completion time for each member (hours: int)
Danyaal Ali: 8
Sanjna Sunil: 12
Silpa Gollapudi: 4.6
Eduardo Pineda: 5
Brandon Manibo: 4

Comments:
For the search functionality, you must click on the "search" button
