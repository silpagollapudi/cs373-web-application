USE disasterfund;
CREATE TABLE IF NOT EXISTS disasters (
	id int not null auto_increment PRIMARY KEY, 
	title varchar(255),
	incident_type varchar(50),
	state varchar(40),
	county_affected varchar(50),
	begin_date varchar(50)
);

CREATE TABLE IF NOT EXISTS projects ( 
	id int not null auto_increment PRIMARY KEY, 
	name varchar(255), 
	city varchar(50), 
	state varchar(40), 
	need text, 
	number_of_donations int, 
	status varchar(10), 
	website text,
	image_medium text,
	image_large text
);

CREATE TABLE IF NOT EXISTS shelters (
  id int not null auto_increment PRIMARY KEY,
  name varchar(255),
  address varchar(255),
  city varchar(50),
  state varchar(50),
  website varchar(255),
  phone varchar(50),
  is_closed varchar(50),
  rating float,
  distance float
);
