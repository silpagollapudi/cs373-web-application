import os

from flask import Flask, jsonify, request
from flask_cors import CORS
from flask_restless import APIManager
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import desc

from db import insert_into_table, drop_table_from
from models import Project, Disaster, Shelter

app = Flask(__name__)

CORS(app)

app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SQLALCHEMY_DATABASE_URI'] = (
        "mysql+pymysql://" +
        os.environ['MYSQL_USER'] +
        ":" +
        os.environ['MYSQL_PASS'] +
        "@dfinstance.clwfvsclezv9.us-east-2.rds.amazonaws.com/disasterfund")

db = SQLAlchemy(app)

manager = APIManager(app, flask_sqlalchemy_db=db)

manager.create_api(Project)
manager.create_api(Disaster)
manager.create_api(Shelter)

default_shelter = [
    {
        'address': '1140 Pacific St',
        'city': 'Brooklyn',
        'distance': 3090.53,
        'id': 1,
        'is_closed': '0',
        'name': 'Peter Young Shelter',
        'phone': '(718) 638-1558',
        'rating': 3.0,
        'state': 'NY',
        'website': 'https://www.yelp.com/biz/peter-young-shelter-brooklyn?adjus'
                   't_creative=6IN38xmtJ--93dNcqPeM-A&utm_campaign=yelp_api_v3&'
                   'utm_medium=api_v3_business_search&utm_source=6IN38xmtJ--93d'
                   'NcqPeM-A'
    }
]
default_project = [
    {
        'image_medium': 'https://www.globalgiving.org/pfil/6949/pict_med.jpg',
        'number_of_donations': 342,
        'city': 'Carlsbad',
        'need': 'Of the more than 100,000 refugees currently living in San '
                'Diego county, over a half have limited or no English skills, '
                'and over a third are not literate in any language. Many '
                'bring physical disabilities and other health issues, and many '
                'have suffered severe traumas. Newcomers often have never '
                'lived in a city, used money or electricity, or ridden in a '
                'car before their journey from refugee camp to San Diego. '
                'Solving basic need, health and access issues helps those '
                'refugees adapt to their new life',
        'id': 1,
        'website': 'https://www.globalgiving.org/projects/refugee-self-reliance'
                   '-california/',
        'state': 'CA',
        'name': 'Bridge to Self Reliance 2000 refugees San Diego CA',
        'image_large': 'https://www.globalgiving.org/pfil/6949/pict_grid7.jpg',
        'status': 'active'
    }
]
default_disaster = [
    {
        'state': 'VA',
        'incident_type': 'Hurricane',
        'county_affected': 'Halifax (County)',
        'title': 'HURRICANE FLORENCE',
        'begin_date': '2018-10-15T09:10:00.000Z',
        'id': 2295
    }
]


def get_disaster_data(disaster):
    """
    Returns attributes for each disaster
    """
    return {
        'id': disaster.id,
        'title': disaster.title,
        'incident_type': disaster.incident_type,
        'state': disaster.state,
        'county_affected': disaster.county_affected,
        'begin_date': disaster.begin_date
    }


def get_project_data(project):
    """
    Returns attributes for each project
    """
    return {
        'id': project.id,
        'name': project.name,
        'city': project.city,
        'state': project.state,
        'need': project.need,
        'number_of_donations': project.number_of_donations,
        'status': project.status,
        'website': project.website,
        'image_medium': project.image_medium,
        'image_large': project.image_large
    }


def get_shelter_data(shelter):
    """
    Returns attributes for each shelter
    """
    return {
        'id': shelter.id,
        'name': shelter.name,
        'address': shelter.address,
        'city': shelter.city,
        'state': shelter.state,
        'website': shelter.website,
        'phone': shelter.phone,
        'is_closed': shelter.is_closed,
        'rating': shelter.rating,
        'distance': shelter.distance
    }


@app.route('/disasters')
def get_disasters():
    """
    Returns every disaster in the United States from the past 5 years. User can sort by:
    year, state, or type.
    """
    sorted_by = request.args.get('sortedBy')
    if sorted_by is not None:
        if sorted_by == "title":
            off = 4
        elif sorted_by == "county_affected":
            off = 2
        elif sorted_by == "begin_date":
            off = 15
            disasters = [get_disaster_data(d) for d in
                         Disaster.query.order_by(desc(sorted_by)).offset(off)]
            return jsonify(disasters)
        else:
            off = 0
        disasters = [get_disaster_data(
            d) for d in Disaster.query.order_by(sorted_by).offset(off)]
    else:
        disasters = [get_disaster_data(d) for d in Disaster.query.all()]
    return jsonify(disasters)


@app.route('/disaster/<id>')
def get_disaster(id):
    """
    Return the natural disaster correlating to the given specific id.
    """
    disaster = Disaster.query.filter_by(id=id).first_or_404()
    return jsonify(get_disaster_data(disaster))


@app.route('/projects')
def get_projects():
    """
    Returns all charities and projects related to natural disasters within the past 5 years.
    """
    sorted_by = request.args.get('sortedBy')
    if sorted_by is not None:
        if sorted_by == "number_of_donations":
            projects = [
                get_project_data(p) for p in Project.query.order_by(
                    desc(sorted_by))]
            return jsonify(projects)
        projects = [get_project_data(p)
                    for p in Project.query.order_by(sorted_by)]
    else:
        projects = [get_project_data(p) for p in Project.query.all()]
    return jsonify(projects)


@app.route('/project/<id>')
def get_project(id):
    """
    Return the charity/project correlating to the given specific id.
    """
    project = Project.query.filter_by(id=id).first_or_404()
    return jsonify(get_project_data(project))


@app.route('/shelters')
def get_shelters():
    """
    Returns all disaster relief shelters in the United States.
    """
    sorted_by = request.args.get('sortedBy')
    if sorted_by is not None:
        if sorted_by == "rating":
            shelters = [
                get_shelter_data(s) for s in Shelter.query.group_by(
                    Shelter.address).order_by(
                    desc(sorted_by))]
            return jsonify(shelters)
        shelters = [
            get_shelter_data(s) for s in Shelter.query.group_by(
                Shelter.address).order_by(sorted_by)]
    else:
        shelters = [
            get_shelter_data(s) for s in Shelter.query.group_by(
                Shelter.address).all()]
    return jsonify(shelters)


@app.route('/shelter/<id>')
def get_shelter(id):
    """
    Return the shelter correlating to the given specific id.
    """
    shelter = Shelter.query.filter_by(id=id).first_or_404()
    return jsonify(get_shelter_data(shelter))


@app.route('/disastersByProject/<ProjectId>')
def get_disasters_by_project(ProjectId):
    """
    Given a specific project id, returns all disasters that occured within the same state.
    """
    project = Project.query.filter_by(id=ProjectId).first_or_404()
    state = project.state
    disasters = [get_disaster_data(d)
                 for d in Disaster.query.filter_by(state=state)]
    if len(disasters) == 0:
        return jsonify(default_disaster)
    return jsonify(disasters)


@app.route('/disastersByShelter/<ShelterId>')
def get_disasters_by_shelter(ShelterId):
    """
    Given a specific shelter id, returns all disasters that occured within the same state.
    """
    shelter = Shelter.query.filter_by(id=ShelterId).first_or_404()
    state = shelter.state
    disasters = [get_disaster_data(d)
                 for d in Disaster.query.filter_by(state=state)]
    if len(disasters) == 0:
        return jsonify(default_disaster)
    return jsonify(disasters)


@app.route('/projectsByDisaster/<DisasterId>')
def get_projects_by_disaster(DisasterId):
    """
    Given a specific disaster id, returns all charities/projects that are within the same state.
    """
    disaster = Disaster.query.filter_by(id=DisasterId).first_or_404()
    state = disaster.state
    projects = [get_project_data(p)
                for p in Project.query.filter_by(state=state)]
    if len(projects) == 0:
        return jsonify(default_project)
    return jsonify(projects)


@app.route('/projectsByShelter/<ShelterId>')
def get_projects_by_shelter(ShelterId):
    """
    Given a specific shelter id, returns all projects that are within the same state.
    """
    shelter = Shelter.query.filter_by(id=ShelterId).first_or_404()
    state = shelter.state
    projects = [get_project_data(p)
                for p in Project.query.filter_by(state=state)]
    if len(projects) == 0:
        return jsonify(default_project)
    return jsonify(projects)


@app.route('/sheltersByDisaster/<DisasterId>')
def get_shelters_by_disaster(DisasterId):
    """
    Given a specfic disaster id, returns all shelters that are located within the same state.
    """
    disaster = Disaster.query.filter_by(id=DisasterId).first_or_404()
    state = disaster.state
    shelters = [get_shelter_data(s)
                for s in Shelter.query.filter_by(state=state)]
    if len(shelters) == 0:
        return jsonify(default_shelter)
    return jsonify(shelters)


@app.route('/sheltersByProject/<ProjectId>')
def get_shelters_by_project(ProjectId):
    """
    Given a specific project id, returns all shelters that are located within the same state.
    """
    project = Project.query.filter_by(id=ProjectId).first_or_404()
    state = project.state
    shelters = [get_shelter_data(s)
                for s in Shelter.query.filter_by(state=state)]
    if len(shelters) == 0:
        return jsonify(default_shelter)
    return jsonify(shelters)


@app.route('/disasters/filterBy')
def get_filtered_disasters():
    """
    Returns all disasters with a given specific attribute.
    """
    title = request.args.get('title')
    incident_type = request.args.get('incident_type')
    county_affected = request.args.get('county_affected')
    state = request.args.get('state')
    begin_date = request.args.get('begin_date')
    if title is not None:
        disasters = [get_disaster_data(d) for d in
                     Disaster.query.filter_by(title=title)]
    elif incident_type is not None:
        disasters = [get_disaster_data(d) for d in
                     Disaster.query.filter_by(incident_type=incident_type)]
    elif county_affected is not None:
        disasters = [get_disaster_data(d) for d in
                     Disaster.query.filter_by(county_affected=county_affected)]
    elif state is not None:
        disasters = [
            get_disaster_data(d) for d in Disaster.query.filter_by(
                state=state)]
    elif begin_date is not None:
        disasters = [get_disaster_data(d) for d in
                     Disaster.query.filter(Disaster.begin_date >= begin_date)]
    return jsonify(disasters)


@app.route('/shelters/filterBy')
def get_filtered_shelters():
    """
    Returns all shelters that have the given specific attribute
    """
    title = request.args.get('name')
    city = request.args.get('city')
    state = request.args.get('state')
    distance = request.args.get('distance')
    rating = request.args.get('rating')
    if title is not None:
        shelters = [
            get_shelter_data(s) for s in Shelter.query.group_by(
                Shelter.address).filter_by(
                name=title)]
    elif city is not None:
        shelters = [
            get_shelter_data(s) for s in Shelter.query.group_by(
                Shelter.address).filter_by(
                city=city)]
    elif state is not None:
        shelters = [
            get_shelter_data(s) for s in Shelter.query.group_by(
                Shelter.address).filter_by(
                state=state)]
    elif distance is not None:
        shelters = [
            get_shelter_data(s) for s in Shelter.query.group_by(
                Shelter.address).order_by('distance').filter(
                Shelter.distance <= distance)]
    elif rating is not None:
        shelters = [
            get_shelter_data(s) for s in Shelter.query.group_by(
                Shelter.address).filter_by(
                rating=rating)]
    return jsonify(shelters)


@app.route('/projects/filterBy')
def get_filtered_projects():
    """
    Given a specific project id, returns all shelters that are located within the same state.
    """
    title = request.args.get('name')
    city = request.args.get('city')
    state = request.args.get('state')
    status = request.args.get('status')
    number_of_donations = request.args.get('number_of_donations')
    if title is not None:
        projects = [get_project_data(p)
                    for p in Project.query.filter_by(name=title)]
    elif city is not None:
        projects = [get_project_data(p)
                    for p in Project.query.filter_by(city=city)]
    elif state is not None:
        projects = [get_project_data(p)
                    for p in Project.query.filter_by(state=state)]
    elif status is not None:
        projects = [
            get_project_data(p) for p in Project.query.filter_by(
                status=status)]
    elif number_of_donations is not None:
        projects = [
            get_project_data(p) for p in Project.query.order_by('number_of_donations').filter(
                Project.number_of_donations >= number_of_donations)]
    return jsonify(projects)


def update_table(model):
    """
    updates (inserts all instances for) specified model table in database
    """
    insert_into_table(model)


def drop_table(model):
    """
    drop table from database
    """
    drop_table_from(model)


@app.route('/')
def api_welcome_message():
    """
    display welcome message for DisasterFund.me's RESTful API
    """
    return "RESTful API for DisasterFund"


if __name__ == '__main__':
    app.run(host='127.0.0.1', port='5000')
