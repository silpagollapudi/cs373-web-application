#!/usr/bin/env python3

import main as my_app
from models import Disaster, Project, Shelter
from unittest import main, TestCase


class TestApi(TestCase):

    def setUp(self):
        my_app.app.config['TESTING'] = True
        self.app = my_app.app.test_client()

    def test_get_disaster_data(self):
        """
        checks if disaster instance has all correct attributes
        """
        d = Disaster()
        d.id = 1
        d.state = "state"
        d.incident_type = "incident_type"
        d.county_affected = "county"
        d.begin_date = "1"
        output = my_app.get_disaster_data(d)
        self.assertEqual(output, {
            'id': d.id,
            'title': d.title,
            'incident_type': d.incident_type,
            'state': d.state,
            'county_affected': d.county_affected,
            'begin_date': d.begin_date
        })

    def test_get_project_data(self):
        """
        checks if project instance has all correct attributes
        """
        p = Project()
        p.id = 1
        p.name = "project"
        p.city = "city"
        p.state = "state"
        p.need = "need"
        p.number_of_donations = "numDonations"
        p.status = "status"
        p.website = "website"
        p.image_medium = "image_medium"
        p.image_large = "image_large"
        output = my_app.get_project_data(p)
        self.assertEqual(output, {
            'id': p.id,
            'name': p.name,
            'city': p.city,
            'state': p.state,
            'need': p.need,
            'number_of_donations': p.number_of_donations,
            'status': p.status,
            'website': p.website,
            'image_medium': p.image_medium,
            'image_large': p.image_large
        })

    def test_get_shelter_data(self):
        """
        checks if disaster instance has all correct attributes
        """
        s = Shelter()
        s.id = 1
        s.name = "name"
        s.address = "address"
        s.city = "city"
        s.state = "state"
        s.website = "website"
        s.phone = "phone"
        s.is_closed = "is_closed"
        s.rating = 1.5
        s.distance = 2.5
        output = my_app.get_shelter_data(s)
        self.assertEqual(output, {
            'id': s.id,
            'name': s.name,
            'address': s.address,
            'city': s.city,
            'state': s.state,
            'website': s.website,
            'phone': s.phone,
            'is_closed': s.is_closed,
            'rating': s.rating,
            'distance': s.distance
        })

    def test_get_disasters(self):
        """
        checks if first instance of all disasters endpoint is correct
        """
        response = self.app.get("/disasters")
        expected_disaster = {'title': 'SOCKEYE FIRE',
                             'county_affected': 'Matanuska-Susitna (Borough)',
                             'incident_type': 'Fire', 'state': 'AK',
                             'begin_date': '2015-06-15T22:37:00.000Z', 'id': 1}
        self.assertEqual(response.json[0], expected_disaster)

    def test_get_disaster(self):
        """
        checks if instance of disasters by id endpoint is correct
        """
        response = self.app.get("/disaster/1")
        expected_disaster = {
            'state': 'AK',
            'incident_type': 'Fire',
            'county_affected': 'Matanuska-Susitna (Borough)',
            'title': 'SOCKEYE FIRE',
            'begin_date': '2015-06-15T22:37:00.000Z',
            'id': 1}
        self.assertEqual(response.json, expected_disaster)

    def test_get_projects(self):
        """
        checks if first instance of all projects endpoint is correct
        """
        response = self.app.get("/projects")
        expected_project = {
            'image_medium': 'https://www.globalgiving.org/pfil/6949/pict_med.jpg',
            'number_of_donations': 342,
            'city': 'Carlsbad',
            'need': 'Of the more than 100,000 refugees currently living in San '
            'Diego county, over a half have limited or no English skills, '
            'and over a third are not literate in any language.  Many '
            'bring physical disabilities and other health issues, and many '
            'have suffered severe traumas. Newcomers often have never '
            'lived in a city, used money or electricity, or ridden in a '
            'car before their journey from refugee camp to San Diego. '
            'Solving basic need, health and access issues helps those '
            'refugees adapt to their new life',
            'id': 1,
            'website': 'https://www.globalgiving.org/projects/refugee-self-reliance'
            '-california/',
            'state': 'CA',
            'name': 'Bridge to Self Reliance 2000 refugees San Diego CA',
            'image_large': 'https://www.globalgiving.org/pfil/6949/pict_grid7.jpg',
            'status': 'active'}
        self.assertEqual(response.json[0], expected_project)

    def test_get_project(self):
        """
        checks if instance of disasters by id endpoint is correct
        """
        response = self.app.get("/project/1")
        expected_project = {
            'image_medium': 'https://www.globalgiving.org/pfil/6949/pict_med.jpg',
            'number_of_donations': 342,
            'city': 'Carlsbad',
            'need': 'Of the more than 100,000 refugees currently living in San '
            'Diego county, over a half have limited or no English skills, '
            'and over a third are not literate in any language.  Many '
            'bring physical disabilities and other health issues, and many '
            'have suffered severe traumas. Newcomers often have never '
            'lived in a city, used money or electricity, or ridden in a '
            'car before their journey from refugee camp to San Diego. '
            'Solving basic need, health and access issues helps those '
            'refugees adapt to their new life',
            'id': 1,
            'website': 'https://www.globalgiving.org/projects/refugee-self-reliance'
            '-california/',
            'state': 'CA',
            'name': 'Bridge to Self Reliance 2000 refugees San Diego CA',
            'image_large': 'https://www.globalgiving.org/pfil/6949/pict_grid7.jpg',
            'status': 'active'}
        self.assertEqual(response.json, expected_project)

    def test_get_shelters(self):
        """
        checks if first instance of all shelters endpoint is correct
        """
        response = self.app.get("/shelters")
        expected_shelter = {
            'address': None,
            'city': 'New York City',
            'distance': 7220.59,
            'id': 2,
            'is_closed': '0',
            'name': 'BRC Project Rescue Homeless Outreach',
            'phone': '(212) 533-5151',
            'rating': 3.0,
            'state': 'NY',
            'website': 'https://www.yelp.com/biz/brc-project-rescue-homeless-outrea'
            'ch-new-york-city?adjust_creative=6IN38xmtJ--93dNcqPeM-A&utm'
            '_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm'
            '_source=6IN38xmtJ--93dNcqPeM-A'}
        self.assertEqual(response.json[0], expected_shelter)

    def test_get_shelter(self):
        """
        checks if instance of shelters by id endpoint is correct
        """
        response = self.app.get("/shelter/1")
        expected_shelter = {
            'address': '1140 Pacific St',
            'city': 'Brooklyn',
            'distance': 3090.53,
            'id': 1,
            'is_closed': '0',
            'name': 'Peter Young Shelter',
            'phone': '(718) 638-1558',
            'rating': 3.0,
            'state': 'NY',
            'website': 'https://www.yelp.com/biz/peter-young-shelter-brooklyn?adjus'
            't_creative=6IN38xmtJ--93dNcqPeM-A&utm_campaign=yelp_api_v3&'
            'utm_medium=api_v3_business_search&utm_source=6IN38xmtJ--93d'
            'NcqPeM-A'}
        self.assertEqual(response.json, expected_shelter)


if __name__ == "__main__":  # pragma: no cover
    main()