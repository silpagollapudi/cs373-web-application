import os

from pymysql import connect
from scrape import get_all_disasters, get_all_projects, get_all_shelters


def insert_into_table(model):
    """
    Gets all instances for given 'model' and inserts them into its corresponding table
    """
    db = connect(host='dfinstance.clwfvsclezv9.us-east-2.rds.amazonaws.com',
                 user=os.environ['MYSQL_USER'],
                 passwd=os.environ['MYSQL_PASS'],
                 database='disasterfund')
    cursor = db.cursor()

    if model == "disasters":
        disasters = get_all_disasters()
        for d in disasters:
            sql = "INSERT INTO disasters VALUES (NULL, %s, %s, %s, %s, %s)"
            val = (
                d['title'],
                d['incident_type'],
                d['state'],
                d['county_affected'],
                d['begin_date'])
            cursor.execute(sql, val)

    elif model == "projects":
        projects = get_all_projects()
        for p in projects:
            sql = "INSERT INTO projects VALUES (NULL, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
            val = (
                p['name'],
                p['city'],
                p['state'],
                p['need'],
                p['number_of_donations'],
                p['status'],
                p['website'],
                p['image_medium'],
                p['image_large'])
            cursor.execute(sql, val)

    elif model == "shelters":
        shelters = get_all_shelters()
        for s in shelters:
            sql = "INSERT INTO shelters VALUES (NULL, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
            val = (
                s['name'],
                s['address'],
                s['city'],
                s['state'],
                s['website'],
                s['phone'],
                s['is_closed'],
                s['rating'],
                s['distance'])
            cursor.execute(sql, val)

    db.commit()
    cursor.close()
    db.close()


def drop_table_from(model):
    """
    Drops the table for a given model
    """
    db = connect(host='dfinstance.clwfvsclezv9.us-east-2.rds.amazonaws.com',
                 user=os.environ['MYSQL_USER'],
                 passwd=os.environ['MYSQL_PASS'],
                 database='disasterfund')
    cursor = db.cursor()

    sql = "DROP TABLE " + model
    cursor.execute(sql)

    db.commit()
    cursor.close()
    db.close()
