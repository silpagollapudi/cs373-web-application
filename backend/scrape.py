import json
import math
import os

import requests


def get_all_disasters():
    """
    Return all disaster since 2015 as a json file
    """
    my_json = []
    skip = 0
    url = (
            "http://www.fema.gov/api/open/v1/DisasterDeclarationsSummaries?" +
            "$format=json&" +
            "$inlinecount=allpages&" +
            "$skip={}&" +
            "$top=1000&" +
            "$orderby=declarationDate&" +
            "$select=declarationDate,declaredCountyArea,incidentType,state,title&" +
            "$filter=(declarationDate gt '2015-01-01T04:00:00.000z') and (" +
            "incidentType eq 'Hurricane' or " +
            "incidentType eq 'Earthquake' or " +
            "incidentType eq 'Fire')")

    response = requests.get(str.format(url, skip))
    skip += 1000

    json_response = response.json()
    my_json = disasters_helper(json_response, my_json)

    if 'metadata' in json_response:
        metadata = json_response['metadata']
        count = metadata['count']

    loop_count = math.ceil(count / 1000)

    # responses = [json_response]
    for _ in range(loop_count - 1):
        response = requests.get(str.format(url, skip))
        json_response = response.json()
        my_json = disasters_helper(json_response, my_json)
        skip += 1000

    return my_json


def disasters_helper(json_response, my_json):
    """
    Helper method for creating our own json file with desired attributes for each disaster
    """
    if 'DisasterDeclarationsSummaries' in json_response:
        disasters = json_response['DisasterDeclarationsSummaries']
        for disaster in disasters:
            d = {}
            if 'title' in disaster:
                d['title'] = disaster['title']
            if 'incidentType' in disaster:
                d['incident_type'] = disaster['incidentType']
            if 'declaredCountyArea' in disaster:
                d['county_affected'] = disaster['declaredCountyArea']
            if 'state' in disaster:
                d['state'] = disaster['state']
            if 'declarationDate' in disaster:
                d['begin_date'] = disaster['declarationDate']

            my_json.append(d)
    return my_json


def get_all_projects():
    """
    Return all active disaster-related charities/projects in the US
    """
    my_json = []
    url = (
            "https://api.globalgiving.org/api/public/projectservice/themes/disaster/projects/" +
            "active?api_key=" +
            os.environ['GLOBAL_GIVING_API_KEY'])
    response = requests.get(url, headers={"Accept": "application/json"})
    json_response = response.json()

    if 'projects' in json_response:
        my_json = projects_helper(json_response, my_json)
    url += "&nextProjectId={}"

    if 'projects' in json_response:
        projects = json_response['projects']
        while 'hasNext' in projects:
            if 'nextProjectId' in projects:
                response = requests.get(
                    str.format(
                        url, projects['nextProjectId']), headers={
                        "Accept": "application/json"})
                json_response = response.json()

                if 'projects' in json_response:
                    my_json = projects_helper(json_response, my_json)
                    projects = json_response['projects']
            else:
                projects = {}

    return my_json


def projects_helper(json_response, my_json):
    """
    Helper method for creating our own json file with desired attributes for each project
    """
    if 'projects' in json_response:
        projects = json_response['projects']
        if 'project' in projects:
            for project in projects['project']:
                if project['country'] == "United States":
                    d = {'status': "active"}
                    if 'title' in project:
                        d['name'] = project['title']
                    if 'numberOfDonations' in project:
                        d['number_of_donations'] = project['numberOfDonations']
                    if 'need' in project:
                        d['need'] = project['need']
                    if 'projectLink' in project:
                        d['website'] = project['projectLink']
                    if 'contactCity' in project:
                        d['city'] = project['contactCity']
                    if 'contactState' in project:
                        d['state'] = project['contactState']
                    if 'image' in project:
                        if 'imagelink' in project['image']:
                            e = project['image']['imagelink']
                            for img in e:
                                if 'size' in img:
                                    if img['size'] == "medium":
                                        d['image_medium'] = img['url']
                                    elif img['size'] == "large":
                                        d['image_large'] = img['url']

                    my_json.append(d)

    return my_json


def get_all_shelters():
    """
    Return all shelters from the top 1000 US cities
    """
    my_json = []

    # Get latitude and longitude for US Cities
    with open('uscities.json') as json_data:
        cities_json = json.load(json_data)

    url = "https://api.yelp.com/v3/businesses/search?term=homeless&latitude={}&longitude={}"
    auth_token = {
        "Authorization": os.environ['YELP_API_KEY']}
    for d in cities_json:
        latitude = d['lat']
        longitude = d['lng']
        response = requests.get(
            str.format(
                url,
                latitude,
                longitude),
            headers=auth_token)
        json_response = response.json()
        my_json = shelters_helper(json_response, my_json)

    return my_json


def shelters_helper(json_response, my_json):
    """
    Helper method for creating our own json file with desired attributes for each shelter
    """
    count = 0
    if 'businesses' in json_response:
        businesses = json_response['businesses']
        for business in businesses:
            d = {}
            if count > 3:
                return my_json
            if 'name' in business:
                d['name'] = business['name']
            if 'location' in business:
                if 'address1' in business['location']:
                    d['address'] = business['location']['address1']
                if 'city' in business['location']:
                    d['city'] = business['location']['city']
                if 'state' in business['location']:
                    d['state'] = business['location']['state']
            if 'url' in business:
                d['website'] = business['url']
            if 'display_phone' in business:
                d['phone'] = business['display_phone']
            if 'is_closed' in business:
                d['is_closed'] = business['is_closed']
            if 'rating' in business:
                d['rating'] = business['rating']
            if 'distance' in business:
                d['distance'] = business['distance']

            count += 1

            my_json.append(d)

    return my_json
