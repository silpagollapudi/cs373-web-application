from flask import Flask
from flask_sqlalchemy import SQLAlchemy, BaseQuery
from sqlalchemy_searchable import SearchQueryMixin

app = Flask(__name__)

app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SQLALCHEMY_DATABASE_URI'] = (
    "mysql+pymysql://disasterfundme:bedss012@dfinstance." +
    "clwfvsclezv9.us-east-2.rds.amazonaws.com/disasterfund")

db = SQLAlchemy(app)


class DisasterQuery(BaseQuery, SearchQueryMixin):
    pass


class ProjectQuery(BaseQuery, SearchQueryMixin):
    pass


class ShelterQuery(BaseQuery, SearchQueryMixin):
    pass


class Disaster(db.Model):
    id = db.Column(db.Integer, primary_key=True, unique=True)
    title = db.Column(db.String(255))
    incident_type = db.Column(db.String(50))
    state = db.Column(db.String(40))
    county_affected = db.Column(db.String(50))
    begin_date = db.Column(db.String(50))

    query_class = DisasterQuery
    __tablename__ = 'disasters'

    def __repr(self):
        return str.format("<Disaster {}>", self.name)


class Project(db.Model):
    id = db.Column(db.Integer, primary_key=True, unique=True)
    name = db.Column(db.String(255))
    city = db.Column(db.String(50))
    state = db.Column(db.String(40))
    need = db.Column(db.Text)
    number_of_donations = db.Column(db.Integer)
    status = db.Column(db.String(10))
    website = db.Column(db.Text)
    image_medium = db.Column(db.Text)
    image_large = db.Column(db.Text)

    query_class = ProjectQuery
    __tablename__ = 'projects'

    def __repr(self):
        return str.format("<Project {}>", self.name)


class Shelter(db.Model):
    id = db.Column(db.Integer, primary_key=True, unique=True)
    name = db.Column(db.String(255))
    address = db.Column(db.String(255))
    city = db.Column(db.String(50))
    state = db.Column(db.String(50))
    website = db.Column(db.String(255))
    phone = db.Column(db.String(50))
    is_closed = db.Column(db.String(50))
    rating = db.Column(db.Float)
    distance = db.Column(db.Float)

    query_class = ShelterQuery
    __tablename__ = 'shelters'

    def __repr(self):
        return str.format("<Shelter {}>", self.name)
