import React, { Component } from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import logo from './logo.svg';

import Home from "./components/home/home.js";
import About from "./components/about/about.js";
import Disasters from "./components/disasters/disasters.js";
import Disaster from "./components/disasters/disaster.js";
import Shelters from "./components/shelters/shelters.js";
import Shelter from "./components/shelters/shelter.js";
import Organizations from "./components/organizations/organizations.js";
import Organization from "./components/organizations/organization.js";
import Navigation from "./components/navigation/navigation.js";
import Search from "./components/search/search.js";

class App extends Component {

  render() {
    return (
      <BrowserRouter>
        <div>
          <Navigation/>
            <Switch>
              <Route path = "/" component = {Home} exact />
              <Route path = "/about" component = {About} />
              <Route path = "/disasters" component = {Disasters} />
              <Route path = "/shelters" component = {Shelters} />
              <Route path = "/organizations" component = {Organizations} />
              <Route path = "/disaster" component = {Disaster} />
              <Route path = "/shelter" component = {Shelter} />
              <Route path = "/organization" component = {Organization} />
              <Route path = "/search" component = {Search} />

            </Switch>
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
