import React from "react";
import {NavLink} from 'react-router-dom';
import "./navigation.css"

// page for navbar navigation
const Navigation = () => {

  return (
    <div className="topnav">
      <NavLink to="/"> DisasterFund </NavLink>
      <NavLink to="/about"> About </NavLink>
      <NavLink to="/disasters"> Disasters </NavLink>
      <NavLink to="/shelters"> Shelters </NavLink>
      <NavLink to="/organizations"> Organizations </NavLink>
      <NavLink to="/search"> Search </NavLink>
    </div>
  );
};

export default Navigation;
