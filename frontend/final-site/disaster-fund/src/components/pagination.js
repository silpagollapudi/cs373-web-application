import React, { Component } from "react";
import PropTypes from 'prop-types';
import './pagination.css';


const pageAtt = {
    items: PropTypes.array.isRequired,
    first: PropTypes.number,
    num: PropTypes.number,
    pageClick: PropTypes.func.isRequired,
}

const initPage = {
    first: 1,
    num: 6
}


class Pagination extends Component {

    constructor(props) {
        // constructor
        super(props);
        this.state = { pages: {} };
    }

    componentWillMount() {
        // set up the pagination
        if(this.props.items && this.props.items.length) {
            this.changePage(this.props.first);
        }
    }


    componentDidUpdate(prevProps, prevState) {
        // update pagination
        if (this.props.items !== prevProps.items) {
            this.changePage(this.props.first);
        }
    }


    changePage(page) {
        // change the current page we are on
        var { items, num } = this.props;
        var pages = this.state.pages;

        if (page < 1 || page > pages.maxPages) {
            return;
        }

        // get the page we need
        pages = this.getCurrPage(items.length, page, num);

        // get the next page we need
        var newPage = items.slice(pages.index, pages.index2 + 1);

        // update values
        this.setState({ pages: pages });

        // invoke function
        this.props.pageClick(newPage);
    }

    getCurrPage(totalComp, curr, num) {

        // use first page
        if(curr) {
            curr = curr;
        }
        else {
            curr = 1;
        }

        // use last page as 10
        num = num || 9;
        if(num) {
            num = num;
        }
        else {
            num = 9;
        }

        // maximum number of pages
        var maxPages = Math.ceil(totalComp / num);

        var beg, end;
        if (maxPages <= 10) {
            // we have less than 10 pages left
            beg = 1;
            end = maxPages;
        } else {
            // we have more than 10 pages left
            if (curr <= 6) {
                beg = 1;
                end = 10;
            } else if (curr + 4 >= maxPages) {
                beg = maxPages - 9;
                end = maxPages;
            } else {
                beg = curr - 5;
                end = curr + 4;
            }
        }

        // need both indices
        var index = num * (curr - 1);
        var index2 = Math.min(index + num - 1, totalComp - 1);
        // back to default
        if(totalComp === 0) {
            index2 = 1
        };

        // display the pagination numbers
        var pages = [...Array((end + 1) - beg).keys()].map(i => beg + i);

        // return all properties
        return {

            totalComp: totalComp,
            curr: curr,
            num: num,
            maxPages: maxPages,
            beg: beg,
            end: end,
            index: index,
            index2: index2,
            pages: pages
        };
    }

    render() {

        var pages = this.state.pages;

        if (!pages.pages || pages.pages.length <= 1) {
            // we only have 1 page rn
            return 1;
        }

        return (
          <div class = "pag">
                <div className={pages.curr === 1 ? 'disabled' : ''}>
                    <a onClick={() => this.changePage(1)}>First</a>
                </div>
                <div className={pages.curr === 1 ? 'disabled' : ''}>
                    <a onClick={() => this.changePage(pages.curr - 1)}>Prev</a>
                </div>
                {pages.pages.map((page, index) =>
                    <div key={index} className={pages.curr === page ? 'active' : ''}>
                        <a onClick={() => this.changePage(page)}>{page}</a>
                    </div>
                )}
                <div className={pages.curr === pages.maxPages ? 'disabled' : ''}>
                    <a onClick={() => this.changePage(pages.curr + 1)}>Next</a>
                </div>
                <div className={pages.curr === pages.maxPages ? 'disabled' : ''}>
                    <a onClick={() => this.changePage(pages.maxPages)}>Last</a>
                </div>
            </div>
        );
    }
}




Pagination.pageAtt = pageAtt;
Pagination.initPage = initPage;


export default Pagination;
