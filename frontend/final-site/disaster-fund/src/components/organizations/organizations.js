import React, { Component } from "react"
import { Link } from 'react-router-dom';
import Organization from './organization';
import Pagination from './../pagination.js';
import Highlighter from "react-highlight-words";
import ReactGoogleMapImage from 'react-google-map-image';
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import Select from 'react-select';


class Organizations extends Component {

    componentDidMount(){
        // get all the data for organizations
        fetch("http://api.disasterfund.me/projects?sortedBy=name")
        .then(res => res.json())
        .then(json => {

            this.setState({

                organizations: json,
            })
        });
        // sorting by city
        fetch("http://api.disasterfund.me/projects?sortedBy=city")
        .then(res => res.json())
        .then(json => {

            this.setState({

                organizations_city: json,
            })
        });
        // sorting by status
        fetch("http://api.disasterfund.me/projects?sortedBy=status")
        .then(res => res.json())
        .then(json => {

            this.setState({

                organizations_status: json,
            })
        });
        // sorting by state
        fetch("http://api.disasterfund.me/projects?sortedBy=state")
        .then(res => res.json())
        .then(json => {

            this.setState({

                organizations_state: json,
            })
        });
    }

    constructor(props) {
        super(props)

        // current state
        this.state = {
            age: 15,
            clicked: -1,
            organizations: [],
            organizations_city: [],
            organizations_status: [],
            organizations_state: [],
            pageOfItems: [],
            organizationIds: [],
            input: "",
            clicked_search: false,
            clicked_filterName: false,
            clicked_filterCity: false,
            clicked_filterStatus: false,
            clicked_filterState: false,
            clicked_filterDonations: false,
            clicked_sortBy: false,
            searchRes: [],
            filterRes: []
        }
    }

    // helps us filter by name
    selectOrgName(event) {
        // set the name filter to true
        this.setState({clicked_filterName: true, clicked_search:false, clicked_filterDonations: false,
            clicked_filterState: false, clicked_filterStatus: false, clicked_filterCity: false, clicked_sortBy: false });

        // get the proper format for the title
        var temp_title = "";
        for (var i = 0; i < event.target.value.length; i++) {
            if(event.target.value.charAt(i) == " ") {
                temp_title = temp_title + "%20";
            }
            else {
                temp_title = temp_title + event.target.value.charAt(i);
            }
        }
        var final_title = ""
        for (var i = 0; i < temp_title.length; i++) {
            if(i == 0 || temp_title.charAt(i-3) == '%') {
                final_title = final_title + temp_title.charAt(i);
            }
            else {
                final_title = final_title + temp_title.charAt(i).toLowerCase();
            }
        }
        //
        fetch("http://api.disasterfund.me/projects/filterBy?name=" + final_title)
            .then(res => res.json())
            .then(json => {

                this.setState({

                    filterRes: json,
                });
            });
   }

   // helps us filter by city
   selectOrgCity(event) {
        // set the city filter to true
        this.setState({clicked_filterName: false, clicked_search:false, clicked_filterDonations: false,
            clicked_filterState: false, clicked_filterStatus: false, clicked_filterCity: true, clicked_sortBy: false });

        fetch("http://api.disasterfund.me/projects/filterBy?city=" + event.target.value)
            .then(res => res.json())
            .then(json => {

                this.setState({

                    filterRes: json,
                });
            });
   }

   // helps us filter by status
   selectOrgStatus(event) {
        // set the status filter to true
        this.setState({clicked_filterName: false, clicked_search:false, clicked_filterDonations: false,
            clicked_filterState: false, clicked_filterStatus: true, clicked_filterCity: false, clicked_sortBy: false });

        fetch("http://api.disasterfund.me/projects/filterBy?status=" + event.target.value)
            .then(res => res.json())
            .then(json => {

                this.setState({

                    filterRes: json,
                });
            });

   }

   // helps us filter by state
   selectOrgState(event) {
        // set the state filter to true
        this.setState({clicked_filterName: false, clicked_search:false, clicked_filterDonations: false,
            clicked_filterState: true, clicked_filterStatus: false, clicked_filterCity: false, clicked_sortBy: false });

        fetch("http://api.disasterfund.me/projects/filterBy?state=" + event.target.value)
            .then(res => res.json())
            .then(json => {

                this.setState({

                    filterRes: json,
                });
            });

   }
   // helps us filter by number of donations
   selectOrgDonations(event) {
    // set the donations filter to true
    this.setState({clicked_filterName: false, clicked_search:false, clicked_filterDonations: true,
        clicked_filterState: false, clicked_filterStatus: false, clicked_filterCity: false, clicked_sortBy: false });

    fetch("http://api.disasterfund.me/projects/filterBy?number_of_donations=" + event.target.value)
        .then(res => res.json())
        .then(json => {

            this.setState({

                filterRes: json,
            });
        });

   }

    // helps us sort by a certain attribute
   sortBy(event) {
    this.setState({clicked_filterName: false, clicked_search:false, clicked_filterDonations: false,
        clicked_filterState: false, clicked_filterStatus: false, clicked_filterCity: false, clicked_sortBy: true });

    fetch("http://api.disasterfund.me/projects?sortedBy=" + event.target.value)
        .then(res => res.json())
        .then(json => {

            this.setState({

                filterRes: json,
            });
        });
   }

    // go to a certain page
    goToPage(id, e) {
        this.state.clicked = parseInt(id)
    }
    // update state with new page of items
    pageClick(pageOfItems) {
        this.setState({ pageOfItems: pageOfItems });
    };

    // update state with new input
    handleInput(event) {
        this.setState({input: event.target.value});
    }
    // update state when search is submitted
    handleSubmit(event) {
        this.setState({clicked_filterName: false, clicked_search:true, clicked_filterDonations: false,
            clicked_filterState: false, clicked_filterStatus: false, clicked_filterCity: false, clicked_sortBy: false });

        this.clearSearch();
        this.searchOrg(this.state.input);

    }

    // clears the last search
    clearSearch() {

        this.state.organizationIds = [];
        this.state.searchRes = [];
    }

    // helps us search the page
    searchOrg(term) {
        this.state.organizations.map((eachOrg, index) => {
            if(eachOrg.name.toLowerCase().includes(term.toLowerCase()) ||
                eachOrg.city.toLowerCase().includes(term.toLowerCase()) ||
                eachOrg.state.toLowerCase().includes(term.toLowerCase()) ||
                eachOrg.status.toLowerCase().includes(term.toLowerCase()) ||
                eachOrg.number_of_donations.toString().includes(term.toLowerCase()))
            {
                this.state.organizationIds.push(eachOrg.id);
                this.state.searchRes.push(eachOrg);
            }

        })

    }

    render = () => {

        // prevent duplicates names from entering
        var curr_names = [];

              const name_options= (
                this.state.organizations.map((eachOrg, index)=>{

                    if(!curr_names.includes(eachOrg.name)) {
                        curr_names.push(eachOrg.name);

                        return (
                            <option key={eachOrg.id} value= {eachOrg.name}> {eachOrg.name} </option>
                        )
                    }

                }
             )
           );

        // prevent duplicates cities from entering
        var curr_city = [];
        const city_options= (
                this.state.organizations_city.map((eachOrg, index)=>{
                    if(!curr_city.includes(eachOrg.city)) {
                        curr_city.push(eachOrg.city);

                    return (

                            <option key={eachOrg.id} value= {eachOrg.city} > {eachOrg.city} </option>

                    )
                }

                }
                        )
           );

            // prevent duplicates status from entering
            var curr_status = [];
            const status_options= (
                this.state.organizations_status.map((eachOrg, index)=>{
                if(!curr_status.includes(eachOrg.status)) {
                    curr_status.push(eachOrg.status);

                    return (

                            <option key={eachOrg.id} value= {eachOrg.status}> {eachOrg.status} </option>

                    )
                }

                }
                        )
           );

            // prevent duplicates states from entering
            var curr_states = [];
           const state_options= (
                this.state.organizations_state.map((eachOrg, index)=>{
                if(!curr_states.includes(eachOrg.state)) {
                    curr_states.push(eachOrg.state);

                    return (

                            <option key={eachOrg.id} value= {eachOrg.state}> {eachOrg.state} </option>

                    )
                }

                }
                        )
           );

           // prevent duplicates donations from entering
           var curr_donations = [];
           const donations_options= (
                this.state.organizations.map((eachOrg, index)=>{
                if(!curr_donations.includes(eachOrg.number_of_donations)) {
                    curr_donations.push(eachOrg.number_of_donations);

                    return (

                            <option key={eachOrg.id} value= {eachOrg.number_of_donations}> {eachOrg.number_of_donations} </option>

                    )
                }

                }
                        )
           );

        // helps us display searched orgs
        const search_org= (
                this.state.pageOfItems.map((eachOrg, index)=>{
                    var count;

                    for (count = 0; count < this.state.organizationIds.length; count++)
                    {
                        if (eachOrg.id == this.state.organizationIds[count])
                        {

                            var here = "/organization/" + eachOrg.id;
                            var donations = eachOrg.number_of_donations.toString();
                            return (
                            <div class = "profile">
                                <img src={eachOrg.image_medium} style={{width:'311px',height:'200px'}} alt = 'image' ></img>
                                <h1><Highlighter highlightClassName="YourHighlightClass" searchWords={[this.state.input]} autoEscape={true} textToHighlight={eachOrg.name}/></h1>
                                <p>City: <Highlighter highlightClassName="YourHighlightClass" searchWords={[this.state.input]} autoEscape={true} textToHighlight={eachOrg.city}/></p>
                                <p>State: <Highlighter highlightClassName="YourHighlightClass" searchWords={[this.state.input]} autoEscape={true} textToHighlight={eachOrg.state}/></p>
                                <p>Status: <Highlighter highlightClassName="YourHighlightClass" searchWords={[this.state.input]} autoEscape={true} textToHighlight={eachOrg.status}/></p>
                                <p>Number of Donations: <Highlighter highlightClassName="YourHighlightClass" searchWords={[this.state.input]} autoEscape={true} textToHighlight={donations}/></p>

                                <h2><Link to={here}>Read More</Link></h2>

                            </div>
                            )
                        }

                    }

                })
           );

        // display all the orgs
        const org= (

                this.state.pageOfItems.map((eachOrg, index)=>{

                    var here = "/organization/" + eachOrg.id;

                    if(eachOrg.id === 19) {
                        eachOrg.image_medium = "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBw4NDw4QDRAODg4NDQ0NDxAYEBANDQ8NFREWFhURFRUYKDQsGBolJxUVLTEtMTU3Ojo6Fx8zODMsNzQ5OisBCgoKDg0OGRAQGCsdFRkrKy0rLS0tLSstLSsrLSstNy0tKy0rLSstLSstKy0tLSstNystLSsrKy0tKzctLSstK//AABEIAJ8BPgMBIgACEQEDEQH/xAAcAAACAwEBAQEAAAAAAAAAAAAFBgMEBwIBAAj/xABHEAABAgQDBAYFCgUDBAIDAAACAQMABBESBSEiEzEyQQZCUVJhcRQjYoGRB3KCkqGisbLR8BUzwcLhJEPSU2Px8hbiNJOz/8QAGgEAAgMBAQAAAAAAAAAAAAAAAgMAAQQFBv/EACYRAAICAgICAgMAAwEAAAAAAAABAhEDIRIxBEETUQUiYXGBkTP/2gAMAwEAAhEDEQA/AMuBxsgGo3WG2KIiDqFKV1KmVaH9bOKU62Fx2CgJUslK4vd5QbxTCxZlxeSqE7M7FVX+XaAKRWlxZ86/ZyGArX+nVNSVEnEJVEjLeQJav8vki7+LPkikqHzyuSposBhCtqgG82DxutCgkYmxsyH+YRD+CJWPZ3DH5czAEZcVsivQVAuC0u3Ui3fZlHWDNqjqAiMqtCcE6W7JwBIhqSqm9RRN/PygjiPSFCFBeHa287BvH2tVbaaaefmkC5P6sTYKHEX22ybW8LlEXVK7hIbh37uFc/KDck62QODMZvGW/Y6rx1Dq61Ozz5QvFPo4Sg82BA4VynqJxsCIFuFa9g09+eceFMCuzba2oWiInU7bjLSenkvVrzREySKeO/4WOuAdG75cH3mJswEnCBGbCb2WoRMniK0aKIqi0zSlUVIhGdQKpOMGwbo3Ca/OtuISTUOn3ZZpEWH9IHJaQmby2m0/04Ah2tiAhaI29bIutXdu5wnlOvGoITjgCKDTXdaNqDUfd/WLljjNEo0k5htFbQzZQCLZ1ISDZ6tJC23dcq5eVV7Frcdm2ZZaJe6F1146wYusu2giuoVQsueqiJujLpXE3AVu9DMGnBfTXqQu3nlUl5dZc4Oh0vQUVEbvbAhIr024OlXiIVyGtN1cq5Z5wn4XHrZabTGKY6VNq4DdKAr2Tu2ICEQc/mbO0STTdpL2d6wRSYlHlNG3TeVtwSaztuttK0rvEs86b6VjKJx1SQFcStSIk3DUcq8t3Z2ZxYw3Ym5eYqKKQ1QdIgPXJCUkovNE3ZqkPeNUEsjXQ+H0ebnyQjmWpZCuKhGB2haGnTwlQlTdvBc80ijPfJy+1UpOak5y4iEAEyGYcG1SIbS0qtBXnnT3QuYhhQ7IjRpxDbucM1IdTJJoK2iJvIeElXiVUSIGWvQXENxNY27iutIhExISHctCTLzi4xSVIFycns5l5odsgu1ZRStNbBTZldQtNOxEru3RSnMUmHxEHXTMQEREVXSIpuy/fKCmPr6SoTraf/lIQvpRNMwI6i+lv80KAk00QKN6bxEvo7v6QSSTKrRXWD3RGWbfmhZddFkH2nWycIbwHQpVp9FKeNIAxblmivFEXM0yX5wrBskezR5Lo1hMkjpnNekzKCKy9ZYjYE+8TZUuruzXLfDD0d6esBLp/EHXQmR0rRi4bPZIV0p4Uy7Iyxzo1NWme+wtSV1fOjhro9Mm2hNtuHUiSiAXDRNVYFOh8oJ9xNFx7pVhc6Jgc7PgJ6dLYjo6w7lgTKT2CMIghNYiqCNqZN/8YSlwGb/6LqeYWxz/AAh4eNLK9sVy/oKxR64jhNzuCvkBG/ihK2twp6gR+wIhxHHW3XmyZN6UCXEha2ImL43FcRE4RIq180TsRIXf4NMASW76XItFT8Y5fw+YLM1qvn+MRux0OMOka70Z+VEG0QX512Zom5yTEHv/ANgHRfhFXph0iWcYKZSQR2Xut9Y9pLVbwoHD43cuUZXJYM+ZogIta9kaAzhk9sqKUuKFc2X+mbG4CpcJez7oHlvsbCOOUHapiX0rwpZZwUSWWUo0JGFSPiUrSuvJOqtN27dCzGodNMJmxbVHJrbIezqyICIlaK2lp7IQnMNJOXPfB8kjHOH0UWyBOIbvpWx4R51FLffd9sXUw9f2kepIZ0VeH7sV8kQfjkQtYk8PXP3mf6x3/Fpj/qn9Yl/FYnTB17YuSvRozrmtR9m6JziThIFriL65bQ/rLBOSlXJtlRZ9c/clG0zetRcy8otH0UTP1vWtztHX3d8X8N6Ck8i2kqqn0YFzix2LnC69oBB0PxQt0lMr5NkUP3RPoXNnLXTBvSRemiRoVrTbYCI+uEV3l+kAE6HuMl/NcC3UNHLS+jTnDp0V6DyiiExNGcw4JFpI/SAHV3ab/CKnKMlQuMGjNJ7AHUKZJl4XJVh/Yi+RiAOkRUG1OtXfXwVYoT2ETEmaI+3aqgLoVzbcAqahXcW9I23F+kOECjVzrTqjMtjsTbISatK0jtIfVkiFWv6xmXyiYis7M7VhXVYNutNpe2NpIJLSiW1UEWi8qLEjNt0DJRXQvtu7AjIEQ9NtV6wrp09v+IpNvnVbfsSLHp1raNo23UVuVylxlqVefmnwiObeBxEJEo5cV1ERG1TtROXKDoVR0s+4QAFy0BSKq6q3ZZ18FVIuTL7ityrJgAgju0AyHiEqaSLuot2XjA6XmVbrRE4SHPqn30pzSJBnXas6lPYFc0ipVBtVFt8U0pBBHIvLWiroS+1F4efhF3CgbK9HNmFrZOAZqStlptsVO1eS5UVIjaNlUVeA0EsuqREKpaidn6xXnk1CKVWxsRXzzgeyz5SratMrRpw3Wii3Z84sEKqCmOR3bTJBTcS0If8AHhFJt1NxJXK39r+90WheqKoIqq6hy5BbTUNN36Vi6Iiww6FqbStVuI6raJatNOzly7Y6a2SopWkoiVqqtvDmXD2f4ii+aqqCelRS3Klv73RKDhhVFFFRwRoi66klaF55EifutUSztxGB3KaKPercXd080jtqZbFUREurbcnDd3hrzRbt3hyii8CoqIVK29i93KCbWHMqlyOqnVRFbIz1W7hol1EI1X5vOISwnN4I+2gD6EdnCDomLrblw1uurS5La76JzRN0CdoBmoo4Wx4VBDJq5rLSKqi8+8nZFqQkXgNTlqO7O0wcRD2A3Dw50tcyFKe6PZ7AH3XEVEZASvFVRREAIalqy55Uy5okVasGwYEk+6VBJHFW2tDuRdF2dOdBVforziCYZsRFUkJTzp7PIvf+sSBh8wi6bBVP+80n90TSUk60424hMoTTgOCimJpcJISXW1yg0TZJgs0iIrZjtAuFxArapGmWSp4XfGJ+lpMm60TCKIejNjbUitNCW5KlBzpRjQPLIOSLTLKSIo86Aot3pjjiEZkVNQ1BtPshj6cdKZSabbFibI6oVwhLhpO3vEtMq9Xvdu6mqYcHyW9GRbNYM4Dhjzz7AtpcpEP0d+RL1Y9bl1FajWvakOHQRw2Xqi0junUqpdbpW3lC/kvQ3gltBBnB50BdbNo/XCPD62089QkVPxWL/R9l9hzYm1MOoAi46oskQNCVbSIkXTW0vckM+Euzs8qiyjQAJa1ICEW/mlTUv6xxj+IHKTDMnKnxWvTryqA6LhEAEabltotEVURe1axTlW0Erm6KM3Kg4ldyCPJR+7+sB/k5wTaKuIzbAOm+ZbEXdmrbUogp6xBXn403edYZHMPbuMDUBZdbJx0AUtm3oXaNiVP04o8keiU+5spgDYJlyWbBGdQ01KV6dnF91PdbblG0SKjGVSZ7/ArJp1GwaBpxth4QH+W2ZEYmIjuFFsHLxWD38JBbCVEqPKlwfVgtK4OAAKGmzcLjNF6/IaLXKLDmHGKaVrBpOhcpq9AgpFKUtRfdAl2R1arETx1Q0nJlThWBLsoqEt4ql0UyKWhdxXCduqWqiW6cr9Xtc4CTPRNLXVOhqQ5Kt+k/rJDpMS6V0oq6faGKD8jUVQgd1eAEH1roCTJHsy9ei81XQH5R/NBWU6HuAKq4LS3N+0VrtyjaNpJppb274MN4K/tKjtQ1b1UP7khmZkDtW9UNXB32dfvDasC2GxLYwd4ciBlfL81pLqgxhODGjZ7QQqRFbVBHR3bhSCkrh6N9ZVX5ltv4wXlmVEd6r7v8QDbZaATeGqOSiBp4XafrKsJPRDpiAzE0mIE2MuG0FsUEdptKkg80VURN8aoQL7Wn2B/SPzQeGPuEaiBHqKtBIufgkHjUfZTb9Gup0rwwEqL9T7diI3fegj0e6UShuMtCZEbpEIUbIbitUreJe6ufhGHngz48QGO7eBD/AEhi6BYUQzzJncmzIlHQVhaC3l1YKXBLsFOX0MHykMm+9eoIGm0bUsIjytuy1UQS8YWpnotNNMbexLSIc0R24RKmklpQfHOG/F2DcJLU63sl26d8C8RYcqqVULu1bQ4ozLK0FKCYmFIFfUkQdWSKhEA6shWu9KRYYkW1JbwVdO4bkRCyg8OHGVFREXx6n0irHaYeq9X4RfzyYHBCnITiNPE6ypMrcqt6RdEQrwkhb8qRbxvEBfdYdWxbRtK0LSK0lK4k7VugIQKi0VKR8qZe+NwFaJZe1HA2iVBDBTROYVS6L+PuMekuLJIoS9Q2SKtzg6BqKkvYtYHilUpTO4fwiR2VcFK2rSg1VNQ6huH7IhTOrGL20FTVFbHaZVJHaLURTnygxLYINWER1rbmdzrKg+JNNimaFQdSUrWiwCbvbLOokBJvTUJZ9VYty+KONvbWtx7Mm+7xBb9n9IotLQwsYE0Q6XW60FUudlCO5OqQnbb8VjoMAmBLZ3EpEguIAIBhwlquaIrc+awuO4u4a684+anEVdV6V7CgWtF0MuIYPNC4VjhIjykQoQOiQ7iItKcl/FIFzGGzo6tu2SCXFtRBb/IqZ6vtj2XfdbL1D67RUttUyEhHrWl3vfFl+YxB8bXDeVOxT0jdQtOWkYpWkVTB0s5OMVVlSNSEhK0rx1UoVB5ouaLHE1PT6qqmr4LvJUEmuqgru3VtSqdqVgo3JvNiumWVOIkNsSLV7XlFFyds6tlP+m460P2Qa/wVxsCvuuGRE4pKZLUlW65S7V8YgrB0cW5IryeZg7+cY4KdAk1gya+0yIl9y2LtE4sGyrlu0TkbRCXjuIftEfhHUjLk+6yyKoKuug2iktooZEiVVeSboISLQPOI3bKhtSEbzN1ptvV1irpGCXS7ApfD1bRh5h9SUtbcyMyOmnEiClvhFk4suS3RDDVe2RY2wJJb6wZZw2MyonrLkSHDo/8AJlIPuq23jjj5ol3qm9kKj7JKRIXujH2nKZLuUSH/AMQelJxxhAVDqo2kBIpA5pXiFetTl+MVotJm/Yb8moSoqjOJ4uCrzR9sR+rbn74o4p8lquubb+JzCuDwm6yy7bqu1W23ZxkxdKcVmUqy7MuqJEJ3Pu2jpTq3/Oiu85i55mTSJ7SMn9pIqxfEtOS9hTpdiU/hU47Ko+E4GzEtoIEF1w8xqtpIvYvZ5Ic6O/K3iTLTTS4d6QDIi2ijtQK0URM8lzhAcdm20Ulflxt32C0LnutFPxg7gcsEw2bj5Pk4LVyGJO6TLhyGvD/TNItK9INRT3I0xn5TnH0sdwiZoSUWjzRfdKlI7Z6cGDlVbdZZ2dthuMEe1u0kJEXDxb15J25Za7KDPIqPmu2ly2ZOJ/utqOkiResn9Is4Z0YlRJKqa+//ABFpXoLLi+N66fRuuBzbc+0ZI44vUMEMdNwoW8fndsFwkxRLVUjTxVCjNeg01sJ70ZtbWdgNocWtCVeL60aihQDpsXOEoVftFT+EtVrn8Y8PCG1SlT+tFwjomSKqx02SqiKSUXsiqQu2Bf8A4+iLVD3dqfosRuopJS6n784OvFaJL2ISwsuPrTVWvgn+IXNJDItvsqkKgvJfuxZbM6aafG6KCDUtSL9cx/LEwKCLuBLvEfzUjOhhOpuDXNPfbCxhfRNllXa0o5q6hf26fjDI4qDvp5xE9MoA5kie8f33YIqxYxDBwDgVVThzt/RIpSmHghItQFRu3XXcPmsMJup3lJOz/wBYiBGy3gmriqlsLdWQEOtptLSMJddPEj4/S3dseS+BzTxGrGyNCHj0iPs6SRYPM4RL5qSAlw98h+lugxLdHJZxOVBtyFwiDT3hpBxxNguQlf8Awyd3mxVbrrxNgutdbaS6o7Z6KTYotzLYKq9YmwXy0lDs/wBFAVVMHSbUuzS3w91KJHjPRkBSivL9GiV8VuVc/KkMWFA8j81zzN000jgol1uWrUFS1RPiMu2LaqIIqd6ttpcIx1jEsoEw7UFzHhNt3iLhuGqIvFF1+a9SYIi1cbcFOHjt84K+ghWkwrcmSLsy39vLPkq7k84mlyUEQgWilvraVoiV1w14cxX4xM+NrrFhDXZMX6rbTHSQkXVXQn2RC+aAZZKmzccJF4SMLqD7/wDMMewKPkl9otxrrcuL6V3ZERShXUGlw706v7WCb7qCrIkK6Qu+sKEVyImrwzio+VpIiJT6vdt58MApMLjZVdl7bKLVSRSVETgWuQ15r+sfJJuUJUS6zipdp3UKsTNtXqiVyutGnVu63lEhK83kmSd7vhcve4v/ABBcicWRPSblLyISUrSXWhlq4bvFc4naKcBKARqgjdRFutGC7yTUrLsPONkrEz/KMrNmVqXW2jwkhDz3pWLmIYeatbf0f0cdmOsFv2Z9YiEU9kfKq5wPJ/RW/YvrKzjwuEt1GRbI6rbaBFbf4oi715VSOXMGcaW18rCTaaUFXCK3TaNOa6acqLWsWSVHEVDmCNLrt5ait4iu5x2000aADz6q01tCAFWy27UVpU3qopyWJzD4MXV/ax5BaflWstkSIiD2Fq1ePzoHK0td0GpJlcWg5hkoKmC1Srdv2U1Rz0kZpbnXMu34xaw6VuUVutW0SLLdXfnFnE5G50AE7/VuFVE4bacXh2+EZIzbzJWejlDFHwWnH9vsq9EMFKZK7ZbYRuuG4R02rqzi3PdDpuxFFtEIicq0ikRDbbaNO1bl+EUMOmXpJ+rSohiulajb7JZQ2N4t6tTbdmEmSucor7RN7W1R2hNjqu1fD4Q/9nI5ahD4/wDQM6N4a8wjoqBqZOW0oRFYPDp6vEUFcXwmaVg/VKCEJDVdPEPW7vwiOTffknrzUwR23XxNl3SK2okn2pDtKYzt0VorBe7F/lufNL9++OnHDUVZxsmVqWloxedkX20vmDAkItwmJF860co0vo8a7MFYnjZRxohaCwitd02jbw1TVSibt8L2O4I84jyi1s0bcKjY63BbtTUXeRVqunNMslXKA8tibkihI3RUcER3iRDoTdkuS3VRUpWvwweRjkmdTxJwnEYsXmf9a/c5tjJtoVNEt1iN1u7VRF378o8lXDJckWA+BmgOK6/quQq9Y9XWhxkiA0RQVFSBxS4x2afPwTSjKv1SJujDc0M6D+SIKWWrquqJJ/XsjVpaecpqFPywkdGRXb70QLdX2w1YlizMsNeMy4G04y/4pDMac5VH2c3Pk0r9INNTVUzFfuxOjyftYzed6RmeRkid4B4RPu+15xWexpbUIFVFbtjfHwJtbOfLyVekafNrVp1P+2f5VhOG/rKieymmIuh/TkJ7bS7+iZaErVtt2oJldb2+XKuSUiU0WtRX3LwRzM6cXTNkDpy/Jcq9ip/dFhHrk1fDiiqsfOKoiajmoiRUXv2wgaI3yldOVklSUkS/1K5vmiCRNAQ6QFaZEt1e1EpzXLnov8lz822D87NuMkY3iyNxON3atThLW78O1YzzovKv4hPhMOLtKTbLjxqudxkqp7qj7o/QOBzUyT1hMm0HWUgEderhISW7h7OaZwxy4OkXCHKLYi9J+j2IYC0szKTJTkq2XrWXBucAckvu6yJQeWSeEGeieJtYiwDzQhxEJhURNt3it3auXuVIJdIhm5uWnZVxow2rezR6wBYS4kFBHVcS+6kZz8j97azw3EqCbIq2iKo3Jfr81tp7s+UTUlYMlxNaZwxw11NXJ42fR7ILYbhCMpQEMELiRTu+r3YFy8w4Q2iwRp46bvnEipEwy8yqonosugdW5L7fvfhBRpC2H0BR/wDJR4rKLvT7RWKLaTSUQAlwTwQv6EkStuP01NHX2XBp9pQwE/OfSmVBl2WvMHrXRvsVwuEk0ltETlSlEpvzWke4tIKKGQAqo3cQ1ULx023FbxZ/hB7pPMtvTErkpo05beik0+QCVv8AMvXT2KifCLuI4M4aKbaXqXGlRMLLdWqq3FzrXshNrobWkZE+lVS1FTLtItfWKLx2ONIp3KjSFZlbxCg6i7EURonnyiy5KUVcsri/9YsTEkgpuREPs4YD50hvBFWXW4JciVbkLZ7+oIrbmvDSJjbRXM7FXSKGtxW7/CJGW7ERO6V33YLJgauDeJIt3D+6QDyt9EcaBHogC4mbSWldpQuMeEhLLP7IieYur11EuaDcXVuu6vlB7+EUHPfFf0W0t9fCAc5EUS30aaR5yWF5fUyzm2BtV/3R1WiP0f3WNDxrpRKsy5uzaXrwpS0JgnOqIlTNfsoirGTYniSSpNpsZd4FuIkNCu7NJCqEPuXzrug90SMJt5HlZmHWWNQtuKM3Y+dSUqoKZUEaVpTPPnHTxeRjhg/ZWzLLxp5My4ukKmIYsEw8qvsNMiREQmIXPiGdty3aurVaV3qnYtmWw7NFoioXCvEBaeIY1eadmpmWVVw+W2ZObO1xdr6rP1mlOFVS1E8arCczJnLm9LqFCln3RROK0SK5sULrIgkP4RknNTVrs1vG4asXXMLouScUefw9EXdDQgB1hNfJLi+8kTG0zl6qYLhtSwR4itEeXswME5OkU3QHwTCFcfaBVVoHXNnf1e9ziWcBlxUFhassusjMXKPrbnFHhVNSIv4ovOLeFqw+4D1VRmWnScZZQrTJguIiIey0aZ7l8ofOn3RWQ9BU5cWwdY/1AltC2hNEKiW9aqlCr9FI60fHx4a1bfsCXlTnSb0vRlPSXA1B00bGi3XIKabgt6v1S+EQYRLTT3qnFOwhJsQXvlw/Df7oe8Cx5l9xmXntkbpDaD9QJt0+ERK3hLl8N12ZRJ2UGYNhhravNlaqoljbZkSNiAkXYp3KSJzp1YVj8eaybXQ/LngoaOcAwWbVkFMdqxtSHZLcbdgkQFyW6u/s3L4RTlcOlZp+ZZMTk3mn3W2nFuFvSSiA25JnaPjuh4wvGgk5Ym3RLbMC64Q6RuuNSSilS7iTdCDiOLekqrC5LMv7a+ttr4ipDbb2lbTzryjbBym2mjlNLtHLE6orZNaTEib2ndMeIS/WK2LdDDmlNwCAHSMCaBNLbmm4+WklXOu5c+2sXukEw5ZLz2yTYzzVrof91ugkXs+Hkq88qOEdJUllSy9WS4m142+8TZdZPD8N6hkipRKwznhnyj2KU3Kmwqg4KgY8SKNpQY6LurrH6UPHSs5RWEWeZdscG5hwQ9YJkOm0u1dOnnTdCTgDBsOqB1FbStrbcQZ6ra+yXvRU3xy82PitdHqoefHycDjJVKhvl5hxho3RFVtH6x28IlAfEekdy1EFR8h1Xf7YeyNV1U+Ec9ItoTAWHS10SyUdWkh6vhARlWxSiCSmPIUInNXzY7H4vFD4+Xs8v5bblXosNv8ANVyLlH3pa2qvInNmPtHxFFPZbRTJ4wZBsf5aKLr9/V0jw186+EVfSM0uSwGxKxK8PtF7XF9sdWUlVmWMLZL0Rmj/AIswTZVRuZIj7osXKJ6urkX4RskxMsnk3cp/SJv6RFlGafJ+2npim3QLmnbqXiFmnTpX5q5xoimvbXzUv7o8n5GXlLo6qx8dWdonej4s0pXiEvyxyJRcCVVEuco0nVrxldTqj42wiMW+gm6PzRhGJvyD60Um9YtvBTeImiqKovNFTLsjfU6RK40xYZoqcSo4wx1aWkrv9Iz75TcIbmDcmWG2mwY0uPohCUy4RWgIpWi/pRaqlKM0lh080DJSRK0r7bYlUBIb7bSPVw9sHmi019jMDW76D/SbEZmYkTbkK+mbISAVJraJuuK4dN1K0pz7Im+TXomGFyYtmSG+6W1epw7RRtsTwRLc+ea84KYBIIxqMlddc/3C4nDHiP2a+W61OyLRy2olZdUutYVwkPzbqXJDFBxQick3oLACD2xIlIDNkqrSrqedtn4wQl2+da/ei0AWkSOojz7Y6SLIZJ0wlUcfk7ABPYsAbgEriuz3RF0imrmlQCU1Ed2kmx+bmnLw5QvTr2oD27rq8IqqHp1d4lT35RcNwNmuv0hOqKuF/wDzJUuzjG53Y/j0Kjibhoum66JX6EKJ3Y7dRK7qfv7sStNgeRrb492MTbsfRTaaRVSsN+EozskGxFW0szMRD2bd3Pxhel1yUa5fRhlwt6xujKKukrqWCX0rhW7/ADDcXZU+jh1negqnq7R3GUAHmXLs6Dq7LYOjMLVeNF4c7bvpbo8YEHiVHKIDZbMloJE67b/LHvUuGq9uXbR+PFLLKoi5ZFjjbEvpNIK42BitygttEEs7qafFa7kgr8mOLlKK8PMkQrF0/j7oLdKcAI2/VATStjqvVqT2gCOm4iWglzzTmueeSRPkbbz5o+D6qu22yOidRIqWkla14d3Z2btmbxuOPjewfGzpz5SVJmznj52oSGYGbeTalKADh8rRNUPspATBhOZWYfFy1HX9XjYy2JonhUS+2EdvpPiCMARPWsmYtIdnEOdxbuVFg/0Gc/07qCoBaVyBtLisMrtQrzTn784zwxySb+h+ecXpDE7JuDwO3/TM/pWiUDpqSfKiX7JRLaEdp3WANw8Rd62C7Q2pqXLz/wA6YU+kuMP7XZSxs1dJmXFKXHfkRXW8Oohz9hY0+HBzyXXRlyOlRdd6MzBYUzMK9UNu/mm0F8miNREBGupLhTnurDb0JweYKRYvYR1HBczIxvJq5RG7V2CPKFXE+kz4AxJqDTrUm05wFbf6u24uLPi5c4f+juMTDElKirCNVYEl2m1ChFqLiTtKOpklkUa1dmehDwTo4YrYTTKNtzbY1v1CFxiX0qW/CC+F4c56W6YPpsiJp61FIRG4m3DEirpXSVcu33hWOkbz14CsvUplsTQVG8gMi0jcXzc05r8fH8RmWXpa15GUmZdvahYPq3eE96LqoW+NLjJ/8AYe6TTxmJiSlsGlGjzes39VpN5pSlbeaKtcoDYbhtziEFXnWnGiscD0ZwQtIStEq3aRHnnTnFB/FpgZR1h95DR5wniRWxtFq4dVoimakBZZcOS9tiWxMGUXZklCHmpcBCBCI14eMecZpvJj66NGOEJKn2NOKO+mys0wwiXtbObBaaNQ2mA97T9sZ3KyrDcwCzp0aG65bLm77VtEhLiHt+3KNw6Kz8u82y3s2wc2SOW2jlqUafdy8KQt9MeiLLZoYB6o7su6fd/T/EZXmTTVARg1JWAX+lEkEurEtKsPNu6zZRhnYOGI8RCIolaiO/wgX0dlg9HYKZAEeacJwFRBJwQLqkVdScWWapl2QYYwpAT+Un1x/KqRArSGtAGnglsc9ye0joRqiWdl9s0YKWi0SKi2kNpXCVpdkJQ4RvvMzTaXZLZd7JFDHiM0jIq1zLj/AOMBwfCvj2J/d3Y9F+N8dwx79nJzzcpOiAJVAXK2l3AiWWn3iuVbi98STMhV5GW1Srzdoqt1l5d62veHlBJpGyyJKXc+sMAzmTSZetQ/9M29rRCLiDTb7S8v/Ma8zSg11YnFfKx06M4ImGC6cybKq4IjmBiDdvVEipcq6eXLnFiZ6XyjdbVU7ezT+ZfahYncYN5u8yVPVCVKWjw6oz+am7yVUXKPMywpP9uztxUKTZrOB9OwOeaFUQGiF61OL1tui773LesNgYl6YimKrXaEIp80U/qZfCPz1LI4RorKGpitw2oRFl1so3P5N5mVfYNyYv2wuXE2gkFh3DxbqkpbvJYbHjFaEZqu0d9JZFv+GzAPaUG14eHqzAkI/ARH3wztvAtgovC2P3hT9++K2N4SE6ljTiKzM+rIluEm7iS0hFUz4U86e9C2CdHdgwDUwaPmLYtkSCrQnQbblRF3r5wMuLabFKVRopEe0oo6A6q0K8g72rq8/H3JFJp4zUzrkJEIp9K2DmOMm2FzYqaaq+wnzez8IXJEFBu0t4iNy948oZHaBDOH4ipuWqgU2QuVt1EZEQkO/wBkfjBQJoCWlEr5j+sAcMW1xFrTSQ7ruIhL+2Cb71ErXLyt/NCJqmWELwLnT3xOKwKl5pynAh/TG4fnDnFsZgUyL1a9lwr/AFgbIYHiJg4WgzdDtIBA/u8XxiT0qwaCYn4bBofvdWOJkFMkqgVLTWgj90eL4RI4Bj1WkTtQBH6uXNI5Te3ZsSVA+lV3ZXRaSgpoT7SsKKwsKXDE7aWt+PVz0QlMaeDpXdSCEiyq52p8bbvqwNAlHIs/KLUua+CfV/SCg0mRotT823LCprYiiQiDZXesPu29brc4CzeO+iijEmXrbRbN8VutuK3ZsF2d4+staUTiD9LJg3HkAKqjQ2p88tRF8LU9yxQwuXJx0GhRTcc0tNonE+WkPdUvsjr4Hxjr2ZpRTdv0MADt6oiX3astV0AZjBibcOoFYg7TPIkBa6k8qRo/RnCJFujTs6Jm24Q7FtLCIh0kIk7bcPzULnRVhnn8Bk3FaMGBvbLVe46YmwXG2Qoopnp8qJ5Lu+GUl0Y8nkwi6M0w3o8pys4xdeGy9Ok1Qh1GI6rfNLU9ywKbeekiQi3WtFkvE3cusfaFaQy4cXoD80wiKrO0fEFtEhlgMlEQEqKQlqy3Vr7UMfTfDknZJs5YGQVgdpQGwC5oh1jpTemkqezARxbYTzVX0wWz0jRuSmXDW56WEbR/6gnwH7Q9vl4wGkMWkHXm3FG8m7WUE7G29q4NtyKi1pW4t6Z1XKLs0djKPMq21MtejCoIN4k7eAkQ5JaI3LTfuXdSE7FmX2nH0mWlaN4Hd/DcTiOJaQ8WYqiJX9IkJLHdexj3s0botiWHMzKueiNXCVouKZuuCJEoja24W+pZqmdF5wx4soG44N+j+YACFpjpQrSJRX4ZdlcoxtueQX7HEsvEjPJdLhjdp5pxdnNUpBGUxt80OydmEW4bQvMidtG0tREIjlTLfl8VY+byqzVJ4+Fr6Ckxg0szOYk0CqYNti4FHCIyMhZPq05kUFsTlw9IkJo7bJxx/wBpwtnMIJjdVbeMq2/FUhUMHDnJp/bGq3A211dbwoLfPqov3ImxzDUYKWZJ03XSmJlsAQisvJwRG0iXTW6qx2VerOexlk8NZVmdB5oaSzEkLVf9t0xeJwx7qqpJ8IqyGGrNECNil7bREKKlze1bMRC4e7oEV8FijNy4hMUpfaLIy6pqtaCVERIiHdXkm/mvVhh6FzINzMka5I469Lllp1laIlnpqtvw84HIrg3ZUXTQR+TjEpmZxQlcUlVGHSeRRttpQRG3q50/axpuPsbSXc7RTaJ9H/FYXuh2DtS8zPPNhbe5syJLtRXKRUHzLlDRihUYfVd2wd/IscmbTehzezM5lo87UT4DA1At6tP33YIvO5raqwIxeeUG61rqER+t+l0ZcMHPKl9sfKTUWKuPzq7R3t2lo0+dxfCKUs4jeRKpn3B1fWKJGlR51TcRVRsRyTrGQ28XV60TkN+kF2SdUBQQP63W90eujpUjmdHYTaVz39ndgxhiB6Q005mj7ssRpw6HBRn+0oX2JVGySvet/wCP22x90qnVBoCbVUcNwBBUuEh2ZKVyeKLbCfK/8nZePU1Rtbi4fOCss8MsbBtN1FBRoBuNwdJDS1dFN8Y4ziWDtths5JtVIhzNNrbd2qVa0hW/jswQC2KmrlSGtRK5DJStQUTfUl5+7sIYV0QmX/5ihLhTnqMvIRjz2NxV8/8ARvbT6NG6OS8uT6mwkuDSWsmLTFpXFW3UNtV3ad+/ONMlAUFDYgB7JrQSOWi6BZiVyiqlxdtNSrVV3A8BlJaVkmGG0vAG3GzUk9YRkS3kVOsvb4eFIkw9kVmKgqkGxcZdBLW3B1EQkI5Daue7mvlSJUDNuX+BnJpwxS4GgNbkVUVbgHq2rTen9IIBu31hVwWTNt5HXgQT9ClGyWolbMgT208aFeOfspDGjnNP/rFi6LMYh076UzMjiMxKsbHZBs3MwIiEyAXLCW72uzmkbYJVjB/ltkvR8TYfyQZxga+062VpfdJuCxumMxJctgqY+UB8pnD3kDY7Mpll1BMiB1pzZd7dS37I1D+PoAor7W1Z0+sC4Sb+cI/j9kYxJYML0xJoq3ADW2fFFuIXCcMhCg5jUdkufasPeHYwBvm0BKist3GipxBcg6RLi4hhjimDOlJ0aJhjzLw7VgtqBc0W8fm87YtqQp/MFfrKv4xkWHYnsXldk/UqTjgmyupty0lHh634+/ONXwiYWZbadEKbRkD59bNR09i5QiUeJRhpryrXw7vzSj0QupnT639qR3s0GO3KdqUHhjz9/Z0DggAaZr5on/JY5Ff3144RfKJAdQFReV0SwqOaeK/WiZolyQfhxfmrFh+dZXOxT81tHh7opAqdnlAapWlC0tiAlw9YiraPbl5UjR48YvIr6vZU0+DrsJTzmCJX0wHZeZLV6h4nyM7es04i2184VsaAHzaKTGa2B6QvaFuYcMStK1AIrqLlAqRZMnwFCzdcEVNEO7UuZZp5+Maa46LZIDAogaRaRdVrVojxLmVUASXtWqx1vI8mENx19E8D8dk8h03pdgvD5CaaEDCSbNXCETUiF3YNpwjs0XUiJ+C1pD9g2IobDaiCHxCSK29yJR3p2WxQwSS9KJaCqog5reVq+zb7l+Ec4o+LFRba2p80X2e8SwrJ5ubIk26R0F+J8eM3CKuX9E9+SYdN70UVF0J2swqnpFwXhHaNiQ3WqhFdUtNFy3VYMJQzLYsoitNERVK0dB6QEbuy0VyTktYlkknCW7YtsNrqoIiBEV1dS/GsTA460qOk2BvVJrZiojcHW1KnYI8uVICOd3u6Gz/HxjF8UnJr/hQxyQcCpiKBwkNt9421tIbkS2mmq/qsCpzHHCYtqZvu7Sh1FrZugYEDdwrztdHxqiJ4tOKTrVlHRscUblC0DtuK0biTTnqy8fCEV5VUlJka3aXQqA6LdLmfZuVM+VO2NsZRkrPP5MUoOn2Cp4GplRcm9t6Q8LhoSWgpEhqOzKqLUkUS7OJEzpBbodivo47FhmYoT201KJ3O2oNokIpdwxbSWYHMRQ1buK8FK0XctVwoo3aeSeceMzDji0CtNVyVN0tJXahJUH7saPkhr7EKDCaKb3pLosvAbtpUNmwCPaNlcJFxUEOzevjEmI4lXYG+lXWmnGWmxC0mzMycNwruHiH4DvXJPpaYeNDWhojfEiKICWrSJCgpq7fBFTsiQJNXiMtiF5XWItxGQFQrytpcN13LyjVHJGSX8AaaByOv7JW9mtZkpR43CUbBtlWRMfaJCAskgvh0kklYhrtbbXPVrYV+zIbLiReRF9dN1IkmMDceZSrqVYFzfpDZXabbeLrV86VWlYlw/BHDVbzBpPWCKoBcBV4e6NSp2fDJWTOkqLUbHiR6UMZNMAdEHSRWjcXu3846n8TfcE0RFJFuFURBLR83rQnSOBvsOIW1BUEiyS/Vb1SGkGlfmN1LEIdNF4vnfp4RzWx1IGo1UrQljVfMrvpXdX3e+ELp/iKg+DNADYtiRIKiWsi6xCvFprTxjT5mZBlg1fJ11BG4kELRIO7p4uHP7Uj8/Y7iKzL7zpIgK45dYnA2HVAfJKJ7od4kanyfouTtUGcOd9WnaRbuH2eLuxMTh1VAeRFHiRGb7fnEVfxrAIZ/SgAtNO9LRL6yxzLzyhkFK9q8I/rHaXkRSEfFYVmMSW4Ecpe2REdOEtOkhu5al+EWmHgeW06UbEiqvtCngvtcucDBxJLfWavNLombnUzSiJ9Eb+HTqheaXyQcU+woQUWnQfwBmVlnDK1EvIqqpcWq627qp+/GGVvEAuCpJZtNK923qj+kJUtjKGiCiBeI77B63EJQxzQMuEBMJstpLtuECXEA3abbhrb+EcLNgniqTdm2DjK6VDU0bbiXX+9TIPy0/GnnACemTB4DlldA/wDaNE0uGOrVcuoVS7km+uSwEm8IfFFt0J88Qu+jWBcvh7jZKQFRRISFFuIHDErtRDC3kk19BxguzaMH6QpMihLUFLiSy60/rbvdBqWxFmuTw17NP9qrGPymKTQMmhtaC1ZXCAuiNouXU09i9sM+B9NQJLXiQFHkSCP1SGl3lSGRnaFzxfRp7LmeS5QjfLNgZzuHITWxQ5V4XlM7BoxmJLtC4US4SXtt5wfksYlxbQ1dCwrbVRepHeNTUnNSTwOPBsZlpxm9LT4kUeFeJU7PCC5IzuLT0fn3o/KCyvpM8nqXREmzG8iHdaREPCNN0Oh9H8QxNQ2Kuy0sP+84pBdq7vW/xvgnKYjJ4eIN4e0TxppR1bNPzU6qQSxnH1GXN+caP1Y3WAVoufW4YGWf1EP4ndyPsB6MYZJ0vVZx8SuIi4BPvWj/AFrDc0+pJpqI8kHJIzj5PsbPENs6QINz+zaZHqtCKdbrcWcaOF7SCgiirTNa0FPZTyjNOcr2E4JdGIl5r8I5Wu4d8WjBPuxCTaRyWzSiG9RyJGl87Y5SZtyoHwuiVZdO2nmkR7HxiWWRrMGXDl5aYqkN1c84uKzHbbdM6VhkJBIqSMmjJXWpf1eIbbv1T7Fi67OLtEUkpkQ5LdqXrR4QZ1plduSJlQzSlE7taD+am+Lm23Z6DwPNxQio1Q09CsbRr1JqGycQsqahO5FuWi57vw99+bcbbeURtcOxs1VLBQiVKraicv1WFvCkVvcIKvgA6dXhv+EdYiqqV3MurwnpjRHK4wSYMoYp+Q5xdWt/5CGJ49ZuFUyLNdP75QozGIOmdykqKhFREuu7cvjBYmEJEU0u99x/djhZECzt+Nw3QuU5SNMc+HDF0tg8zccqRkaqYjUU7ntF1q791EiApO1UIaJq6q2l91YYBlUNdypd9Lh+d/Tsj1+SUFT61aj+WvDwxrxzajR5bypc8rl9i4bS8Kbvrf01QTwwTBUUlVeqOshEfqxYodaaF8dIH92LcqyBIqlv8V0/dT+sacckv6ZJFqjeSejpVwbRK4R+kRXJaPFHjKKdUZREXhIEQruHV23VtrktPjBWQblDT16Lf87QW7s/ecXRmQRF2AGa9WiaBD6PD5RpefQniDZNt6ijbVCHhW/VdS78sEZVpxlK7JOHcpv3feT7KV7IpzOJudtLexfu3DETeIzJZJetv0j1U4ruUZ3MLiGjBFT+Si9XcRAID85E8+yK8/MWItQALbRFVDQWnT+UvhHbU4ZpoVdN24buH4Xf1iB2bcGm0MwuIf8AbIdGf28OcS0ShMx50zqlES4daCAj+UtVYQJ7D0ElvF1Fu1U+9pJOL3xrGKreq3vOptOaqY7MLuG0eLL8IW57DQN21skeu4SVNl+ZdPxi1lcdIujORbTaKNpqPKiWll1recE5fBlUdowoP57lXZEPmhQfDC3GSB4FsUS0GikHslaWXepvrEbmFKFRUaai9rrKP0s7oZDya7C4goMDqJK46yr5cLImJHd1R05RVOSdN0wYS9RUROiKQjaNt131vgsNEpgikOVFuK0QRQ2hH7I1u+yLreCIeYVIyJsSRRs15cRcI5l2+dIOXla0i1ATWsFcRdZ7JeVAv6yj26YccFngkRNDUpt61sRXUDYgNC6yXIX6JFfEZVpirYpc7zO8SFv2Rpz8aqnZA9co52byZdHpfxv4eM488vT9BOaxyaeKtwB4IA8PduKJgnFNFQt/amm6Aox23NkC1hEcv2egj4mCCpRQQOZe6hVQt6UjgJ94FyEfqDFZJ1brsqc0TTBNxEVLm1HhIuqJDDIyvoXPBhW3FUePdJ55plxGxlxvG3U2m0QfZqv9ItyPTWYnGlZmGmSfJf5iN2laI9YetCPi0+KEqXEpV574tdHcUVtFMt9bR+bDscZS70ee8vJ4kMlpJ16DE30rcklRthsKjvUwu+0oY8Nxf+PSxyYMmM08oi6+iD6O1LdZwvGmSIvNUhcnsXZmRtMAXzSCGBdMG8OaVlhloLiuI003H7XepDnj49HIzeR8jf60aRh2FYd0Xk3FDaOE4RFqUTmH3eq2FqJ2JuTxWC2FSxPNi9OiO2dS4gTha7Gx8ETnzzWMUlekCHOg++Ru7O6xFW4W7u6PVjXMC6UsTDeRoBDvRcoTkUn60ZkqX9Mxrl+yjgqxJZHSDHENZEDBlW1Kx6AKOcTmleqnD++ceBRc7Rp9L9Ysh0DqFkSJ9UbPnaeGOzbsoQKqfNK36pRIzRUJCsEe8jYkXu3fbErA5ogEuW4uH8INMhRbZu3LTz0xI3L57/ekSk3Ulpnb/wAc48BInJhJ0dbAxSo1oPNE/wCMcq6qpuWviX+IvSsxoJEWldCpbWo/OqnhEbrOdNJeVw/jDHLQUJNO0yJEVOBPndyOmh72+JUap9W6Ogbzikw5ZZP2SIXYldPsxy4JnmaxaQKZV097/wCsdEoonMq71XT91F/rGmMvsxyIGcOqNxElPBCu+buj1wGQppVV03JeX5qf0j5XlJKKq0HdqKOAS5U5Z0u8e3dWC+T6A4l2UmWUrayNfaO7+nbFtlhxwbnDRpnsG3V7IiKpFKUBVJLU0iQmRItpWeFecSzxlci6mxTUmdxfisMU/bBcSUZNs++iauK0T9kbRShV84nR1sG1QCQEMbSCgvuF3eWmKDTrS5qKkdvPhiZx0xS1sUaUrcxtv+tv+2L51sqiEENsrjqiOXFVdJFd8PsiZ98yWt7tBG5oBXR3eHq/ulaRVMiHVeqlWletXtrHYtK8iHpqhZZW184BZfRfErzhAbms1VLitQtdvsl3i09mcV3sPYcvMFsTaWiCkI26eLenPlSPZhtSVaoKeWmJCwstmrlUyVEROaqvLspFKbbCqgdNYaFclBU6tFItHtF3o5l5MN91OK1eG08tQlVNUGGsHcOiZCSbxVf7krHh9H3B3qieS/4gk32QptWf9VmpXXE4HrL+9dRefjlnEWJOy0sy6QGBm5pBtNqQX94rl5dqpyi6/ghAOokVSSvu7IT+kZWOACbrbouWRpG38fhjlzRT6B1fjHixyC849UoxydnvIUlSPCKPqpHCx4qxVAuR0UdOJcNPZtiFTirNTVEhkE70Zs+aEYtyBeIMKhKSrWq713xWSZUUtRco6nJiqxRjqY062eD8rJBZHw6LXpJ9qxyT6rzWK9Y+g6MzysvS06oLnBNjHjHdVPfC+kTCsC1QUJcuz//Z";

                    }

                    return (
                    <div class = "profile">
                        <img src={eachOrg.image_medium} style={{width:'311px',height:'200px'}} alt = 'image' ></img>
                        <h1>{eachOrg.name} </h1>
                        <p>City: {eachOrg.city} </p>
                        <p>State: {eachOrg.state} </p>
                        <p>Status: {eachOrg.status} </p>
                        <p>Number of Donations: {eachOrg.number_of_donations} </p>

                        <h2><Link to={here}>Read More</Link></h2>

                    </div>
                    )
                }
                )
            );

            // displays the final results of the orgs that we have found
            const finalResult= (
                this.state.pageOfItems.map((eachOrg, index)=>{
                var here = "/organization/" + eachOrg.id;

                if(eachOrg.id === 19) {
                eachOrg.image_medium = "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBw4NDw4QDRAODg4NDQ0NDxAYEBANDQ8NFREWFhURFRUYKDQsGBolJxUVLTEtMTU3Ojo6Fx8zODMsNzQ5OisBCgoKDg0OGRAQGCsdFRkrKy0rLS0tLSstLSsrLSstNy0tKy0rLSstLSstKy0tLSstNystLSsrKy0tKzctLSstK//AABEIAJ8BPgMBIgACEQEDEQH/xAAcAAACAwEBAQEAAAAAAAAAAAAFBgMEBwIBAAj/xABHEAABAgQDBAYFCgUDBAIDAAACAQMABBESBSEiEzEyQQZCUVJhcRQjYoGRB3KCkqGisbLR8BUzwcLhJEPSU2Px8hbiNJOz/8QAGgEAAgMBAQAAAAAAAAAAAAAAAgMAAQQFBv/EACYRAAICAgICAgMAAwEAAAAAAAABAhEDIRIxBEETUQUiYXGBkTP/2gAMAwEAAhEDEQA/AMuBxsgGo3WG2KIiDqFKV1KmVaH9bOKU62Fx2CgJUslK4vd5QbxTCxZlxeSqE7M7FVX+XaAKRWlxZ86/ZyGArX+nVNSVEnEJVEjLeQJav8vki7+LPkikqHzyuSposBhCtqgG82DxutCgkYmxsyH+YRD+CJWPZ3DH5czAEZcVsivQVAuC0u3Ui3fZlHWDNqjqAiMqtCcE6W7JwBIhqSqm9RRN/PygjiPSFCFBeHa287BvH2tVbaaaefmkC5P6sTYKHEX22ybW8LlEXVK7hIbh37uFc/KDck62QODMZvGW/Y6rx1Dq61Ozz5QvFPo4Sg82BA4VynqJxsCIFuFa9g09+eceFMCuzba2oWiInU7bjLSenkvVrzREySKeO/4WOuAdG75cH3mJswEnCBGbCb2WoRMniK0aKIqi0zSlUVIhGdQKpOMGwbo3Ca/OtuISTUOn3ZZpEWH9IHJaQmby2m0/04Ah2tiAhaI29bIutXdu5wnlOvGoITjgCKDTXdaNqDUfd/WLljjNEo0k5htFbQzZQCLZ1ISDZ6tJC23dcq5eVV7Frcdm2ZZaJe6F1146wYusu2giuoVQsueqiJujLpXE3AVu9DMGnBfTXqQu3nlUl5dZc4Oh0vQUVEbvbAhIr024OlXiIVyGtN1cq5Z5wn4XHrZabTGKY6VNq4DdKAr2Tu2ICEQc/mbO0STTdpL2d6wRSYlHlNG3TeVtwSaztuttK0rvEs86b6VjKJx1SQFcStSIk3DUcq8t3Z2ZxYw3Ym5eYqKKQ1QdIgPXJCUkovNE3ZqkPeNUEsjXQ+H0ebnyQjmWpZCuKhGB2haGnTwlQlTdvBc80ijPfJy+1UpOak5y4iEAEyGYcG1SIbS0qtBXnnT3QuYhhQ7IjRpxDbucM1IdTJJoK2iJvIeElXiVUSIGWvQXENxNY27iutIhExISHctCTLzi4xSVIFycns5l5odsgu1ZRStNbBTZldQtNOxEru3RSnMUmHxEHXTMQEREVXSIpuy/fKCmPr6SoTraf/lIQvpRNMwI6i+lv80KAk00QKN6bxEvo7v6QSSTKrRXWD3RGWbfmhZddFkH2nWycIbwHQpVp9FKeNIAxblmivFEXM0yX5wrBskezR5Lo1hMkjpnNekzKCKy9ZYjYE+8TZUuruzXLfDD0d6esBLp/EHXQmR0rRi4bPZIV0p4Uy7Iyxzo1NWme+wtSV1fOjhro9Mm2hNtuHUiSiAXDRNVYFOh8oJ9xNFx7pVhc6Jgc7PgJ6dLYjo6w7lgTKT2CMIghNYiqCNqZN/8YSlwGb/6LqeYWxz/AAh4eNLK9sVy/oKxR64jhNzuCvkBG/ihK2twp6gR+wIhxHHW3XmyZN6UCXEha2ImL43FcRE4RIq180TsRIXf4NMASW76XItFT8Y5fw+YLM1qvn+MRux0OMOka70Z+VEG0QX512Zom5yTEHv/ANgHRfhFXph0iWcYKZSQR2Xut9Y9pLVbwoHD43cuUZXJYM+ZogIta9kaAzhk9sqKUuKFc2X+mbG4CpcJez7oHlvsbCOOUHapiX0rwpZZwUSWWUo0JGFSPiUrSuvJOqtN27dCzGodNMJmxbVHJrbIezqyICIlaK2lp7IQnMNJOXPfB8kjHOH0UWyBOIbvpWx4R51FLffd9sXUw9f2kepIZ0VeH7sV8kQfjkQtYk8PXP3mf6x3/Fpj/qn9Yl/FYnTB17YuSvRozrmtR9m6JziThIFriL65bQ/rLBOSlXJtlRZ9c/clG0zetRcy8otH0UTP1vWtztHX3d8X8N6Ck8i2kqqn0YFzix2LnC69oBB0PxQt0lMr5NkUP3RPoXNnLXTBvSRemiRoVrTbYCI+uEV3l+kAE6HuMl/NcC3UNHLS+jTnDp0V6DyiiExNGcw4JFpI/SAHV3ab/CKnKMlQuMGjNJ7AHUKZJl4XJVh/Yi+RiAOkRUG1OtXfXwVYoT2ETEmaI+3aqgLoVzbcAqahXcW9I23F+kOECjVzrTqjMtjsTbISatK0jtIfVkiFWv6xmXyiYis7M7VhXVYNutNpe2NpIJLSiW1UEWi8qLEjNt0DJRXQvtu7AjIEQ9NtV6wrp09v+IpNvnVbfsSLHp1raNo23UVuVylxlqVefmnwiObeBxEJEo5cV1ERG1TtROXKDoVR0s+4QAFy0BSKq6q3ZZ18FVIuTL7ityrJgAgju0AyHiEqaSLuot2XjA6XmVbrRE4SHPqn30pzSJBnXas6lPYFc0ipVBtVFt8U0pBBHIvLWiroS+1F4efhF3CgbK9HNmFrZOAZqStlptsVO1eS5UVIjaNlUVeA0EsuqREKpaidn6xXnk1CKVWxsRXzzgeyz5SratMrRpw3Wii3Z84sEKqCmOR3bTJBTcS0If8AHhFJt1NxJXK39r+90WheqKoIqq6hy5BbTUNN36Vi6Iiww6FqbStVuI6raJatNOzly7Y6a2SopWkoiVqqtvDmXD2f4ii+aqqCelRS3Klv73RKDhhVFFFRwRoi66klaF55EifutUSztxGB3KaKPercXd080jtqZbFUREurbcnDd3hrzRbt3hyii8CoqIVK29i93KCbWHMqlyOqnVRFbIz1W7hol1EI1X5vOISwnN4I+2gD6EdnCDomLrblw1uurS5La76JzRN0CdoBmoo4Wx4VBDJq5rLSKqi8+8nZFqQkXgNTlqO7O0wcRD2A3Dw50tcyFKe6PZ7AH3XEVEZASvFVRREAIalqy55Uy5okVasGwYEk+6VBJHFW2tDuRdF2dOdBVforziCYZsRFUkJTzp7PIvf+sSBh8wi6bBVP+80n90TSUk60424hMoTTgOCimJpcJISXW1yg0TZJgs0iIrZjtAuFxArapGmWSp4XfGJ+lpMm60TCKIejNjbUitNCW5KlBzpRjQPLIOSLTLKSIo86Aot3pjjiEZkVNQ1BtPshj6cdKZSabbFibI6oVwhLhpO3vEtMq9Xvdu6mqYcHyW9GRbNYM4Dhjzz7AtpcpEP0d+RL1Y9bl1FajWvakOHQRw2Xqi0junUqpdbpW3lC/kvQ3gltBBnB50BdbNo/XCPD62089QkVPxWL/R9l9hzYm1MOoAi46oskQNCVbSIkXTW0vckM+Euzs8qiyjQAJa1ICEW/mlTUv6xxj+IHKTDMnKnxWvTryqA6LhEAEabltotEVURe1axTlW0Erm6KM3Kg4ldyCPJR+7+sB/k5wTaKuIzbAOm+ZbEXdmrbUogp6xBXn403edYZHMPbuMDUBZdbJx0AUtm3oXaNiVP04o8keiU+5spgDYJlyWbBGdQ01KV6dnF91PdbblG0SKjGVSZ7/ArJp1GwaBpxth4QH+W2ZEYmIjuFFsHLxWD38JBbCVEqPKlwfVgtK4OAAKGmzcLjNF6/IaLXKLDmHGKaVrBpOhcpq9AgpFKUtRfdAl2R1arETx1Q0nJlThWBLsoqEt4ql0UyKWhdxXCduqWqiW6cr9Xtc4CTPRNLXVOhqQ5Kt+k/rJDpMS6V0oq6faGKD8jUVQgd1eAEH1roCTJHsy9ei81XQH5R/NBWU6HuAKq4LS3N+0VrtyjaNpJppb274MN4K/tKjtQ1b1UP7khmZkDtW9UNXB32dfvDasC2GxLYwd4ciBlfL81pLqgxhODGjZ7QQqRFbVBHR3bhSCkrh6N9ZVX5ltv4wXlmVEd6r7v8QDbZaATeGqOSiBp4XafrKsJPRDpiAzE0mIE2MuG0FsUEdptKkg80VURN8aoQL7Wn2B/SPzQeGPuEaiBHqKtBIufgkHjUfZTb9Gup0rwwEqL9T7diI3fegj0e6UShuMtCZEbpEIUbIbitUreJe6ufhGHngz48QGO7eBD/AEhi6BYUQzzJncmzIlHQVhaC3l1YKXBLsFOX0MHykMm+9eoIGm0bUsIjytuy1UQS8YWpnotNNMbexLSIc0R24RKmklpQfHOG/F2DcJLU63sl26d8C8RYcqqVULu1bQ4ozLK0FKCYmFIFfUkQdWSKhEA6shWu9KRYYkW1JbwVdO4bkRCyg8OHGVFREXx6n0irHaYeq9X4RfzyYHBCnITiNPE6ypMrcqt6RdEQrwkhb8qRbxvEBfdYdWxbRtK0LSK0lK4k7VugIQKi0VKR8qZe+NwFaJZe1HA2iVBDBTROYVS6L+PuMekuLJIoS9Q2SKtzg6BqKkvYtYHilUpTO4fwiR2VcFK2rSg1VNQ6huH7IhTOrGL20FTVFbHaZVJHaLURTnygxLYINWER1rbmdzrKg+JNNimaFQdSUrWiwCbvbLOokBJvTUJZ9VYty+KONvbWtx7Mm+7xBb9n9IotLQwsYE0Q6XW60FUudlCO5OqQnbb8VjoMAmBLZ3EpEguIAIBhwlquaIrc+awuO4u4a684+anEVdV6V7CgWtF0MuIYPNC4VjhIjykQoQOiQ7iItKcl/FIFzGGzo6tu2SCXFtRBb/IqZ6vtj2XfdbL1D67RUttUyEhHrWl3vfFl+YxB8bXDeVOxT0jdQtOWkYpWkVTB0s5OMVVlSNSEhK0rx1UoVB5ouaLHE1PT6qqmr4LvJUEmuqgru3VtSqdqVgo3JvNiumWVOIkNsSLV7XlFFyds6tlP+m460P2Qa/wVxsCvuuGRE4pKZLUlW65S7V8YgrB0cW5IryeZg7+cY4KdAk1gya+0yIl9y2LtE4sGyrlu0TkbRCXjuIftEfhHUjLk+6yyKoKuug2iktooZEiVVeSboISLQPOI3bKhtSEbzN1ptvV1irpGCXS7ApfD1bRh5h9SUtbcyMyOmnEiClvhFk4suS3RDDVe2RY2wJJb6wZZw2MyonrLkSHDo/8AJlIPuq23jjj5ol3qm9kKj7JKRIXujH2nKZLuUSH/AMQelJxxhAVDqo2kBIpA5pXiFetTl+MVotJm/Yb8moSoqjOJ4uCrzR9sR+rbn74o4p8lquubb+JzCuDwm6yy7bqu1W23ZxkxdKcVmUqy7MuqJEJ3Pu2jpTq3/Oiu85i55mTSJ7SMn9pIqxfEtOS9hTpdiU/hU47Ko+E4GzEtoIEF1w8xqtpIvYvZ5Ic6O/K3iTLTTS4d6QDIi2ijtQK0URM8lzhAcdm20Ulflxt32C0LnutFPxg7gcsEw2bj5Pk4LVyGJO6TLhyGvD/TNItK9INRT3I0xn5TnH0sdwiZoSUWjzRfdKlI7Z6cGDlVbdZZ2dthuMEe1u0kJEXDxb15J25Za7KDPIqPmu2ly2ZOJ/utqOkiResn9Is4Z0YlRJKqa+//ABFpXoLLi+N66fRuuBzbc+0ZI44vUMEMdNwoW8fndsFwkxRLVUjTxVCjNeg01sJ70ZtbWdgNocWtCVeL60aihQDpsXOEoVftFT+EtVrn8Y8PCG1SlT+tFwjomSKqx02SqiKSUXsiqQu2Bf8A4+iLVD3dqfosRuopJS6n784OvFaJL2ISwsuPrTVWvgn+IXNJDItvsqkKgvJfuxZbM6aafG6KCDUtSL9cx/LEwKCLuBLvEfzUjOhhOpuDXNPfbCxhfRNllXa0o5q6hf26fjDI4qDvp5xE9MoA5kie8f33YIqxYxDBwDgVVThzt/RIpSmHghItQFRu3XXcPmsMJup3lJOz/wBYiBGy3gmriqlsLdWQEOtptLSMJddPEj4/S3dseS+BzTxGrGyNCHj0iPs6SRYPM4RL5qSAlw98h+lugxLdHJZxOVBtyFwiDT3hpBxxNguQlf8Awyd3mxVbrrxNgutdbaS6o7Z6KTYotzLYKq9YmwXy0lDs/wBFAVVMHSbUuzS3w91KJHjPRkBSivL9GiV8VuVc/KkMWFA8j81zzN000jgol1uWrUFS1RPiMu2LaqIIqd6ttpcIx1jEsoEw7UFzHhNt3iLhuGqIvFF1+a9SYIi1cbcFOHjt84K+ghWkwrcmSLsy39vLPkq7k84mlyUEQgWilvraVoiV1w14cxX4xM+NrrFhDXZMX6rbTHSQkXVXQn2RC+aAZZKmzccJF4SMLqD7/wDMMewKPkl9otxrrcuL6V3ZERShXUGlw706v7WCb7qCrIkK6Qu+sKEVyImrwzio+VpIiJT6vdt58MApMLjZVdl7bKLVSRSVETgWuQ15r+sfJJuUJUS6zipdp3UKsTNtXqiVyutGnVu63lEhK83kmSd7vhcve4v/ABBcicWRPSblLyISUrSXWhlq4bvFc4naKcBKARqgjdRFutGC7yTUrLsPONkrEz/KMrNmVqXW2jwkhDz3pWLmIYeatbf0f0cdmOsFv2Z9YiEU9kfKq5wPJ/RW/YvrKzjwuEt1GRbI6rbaBFbf4oi715VSOXMGcaW18rCTaaUFXCK3TaNOa6acqLWsWSVHEVDmCNLrt5ait4iu5x2000aADz6q01tCAFWy27UVpU3qopyWJzD4MXV/ax5BaflWstkSIiD2Fq1ePzoHK0td0GpJlcWg5hkoKmC1Srdv2U1Rz0kZpbnXMu34xaw6VuUVutW0SLLdXfnFnE5G50AE7/VuFVE4bacXh2+EZIzbzJWejlDFHwWnH9vsq9EMFKZK7ZbYRuuG4R02rqzi3PdDpuxFFtEIicq0ikRDbbaNO1bl+EUMOmXpJ+rSohiulajb7JZQ2N4t6tTbdmEmSucor7RN7W1R2hNjqu1fD4Q/9nI5ahD4/wDQM6N4a8wjoqBqZOW0oRFYPDp6vEUFcXwmaVg/VKCEJDVdPEPW7vwiOTffknrzUwR23XxNl3SK2okn2pDtKYzt0VorBe7F/lufNL9++OnHDUVZxsmVqWloxedkX20vmDAkItwmJF860co0vo8a7MFYnjZRxohaCwitd02jbw1TVSibt8L2O4I84jyi1s0bcKjY63BbtTUXeRVqunNMslXKA8tibkihI3RUcER3iRDoTdkuS3VRUpWvwweRjkmdTxJwnEYsXmf9a/c5tjJtoVNEt1iN1u7VRF378o8lXDJckWA+BmgOK6/quQq9Y9XWhxkiA0RQVFSBxS4x2afPwTSjKv1SJujDc0M6D+SIKWWrquqJJ/XsjVpaecpqFPywkdGRXb70QLdX2w1YlizMsNeMy4G04y/4pDMac5VH2c3Pk0r9INNTVUzFfuxOjyftYzed6RmeRkid4B4RPu+15xWexpbUIFVFbtjfHwJtbOfLyVekafNrVp1P+2f5VhOG/rKieymmIuh/TkJ7bS7+iZaErVtt2oJldb2+XKuSUiU0WtRX3LwRzM6cXTNkDpy/Jcq9ip/dFhHrk1fDiiqsfOKoiajmoiRUXv2wgaI3yldOVklSUkS/1K5vmiCRNAQ6QFaZEt1e1EpzXLnov8lz822D87NuMkY3iyNxON3atThLW78O1YzzovKv4hPhMOLtKTbLjxqudxkqp7qj7o/QOBzUyT1hMm0HWUgEderhISW7h7OaZwxy4OkXCHKLYi9J+j2IYC0szKTJTkq2XrWXBucAckvu6yJQeWSeEGeieJtYiwDzQhxEJhURNt3it3auXuVIJdIhm5uWnZVxow2rezR6wBYS4kFBHVcS+6kZz8j97azw3EqCbIq2iKo3Jfr81tp7s+UTUlYMlxNaZwxw11NXJ42fR7ILYbhCMpQEMELiRTu+r3YFy8w4Q2iwRp46bvnEipEwy8yqonosugdW5L7fvfhBRpC2H0BR/wDJR4rKLvT7RWKLaTSUQAlwTwQv6EkStuP01NHX2XBp9pQwE/OfSmVBl2WvMHrXRvsVwuEk0ltETlSlEpvzWke4tIKKGQAqo3cQ1ULx023FbxZ/hB7pPMtvTErkpo05beik0+QCVv8AMvXT2KifCLuI4M4aKbaXqXGlRMLLdWqq3FzrXshNrobWkZE+lVS1FTLtItfWKLx2ONIp3KjSFZlbxCg6i7EURonnyiy5KUVcsri/9YsTEkgpuREPs4YD50hvBFWXW4JciVbkLZ7+oIrbmvDSJjbRXM7FXSKGtxW7/CJGW7ERO6V33YLJgauDeJIt3D+6QDyt9EcaBHogC4mbSWldpQuMeEhLLP7IieYur11EuaDcXVuu6vlB7+EUHPfFf0W0t9fCAc5EUS30aaR5yWF5fUyzm2BtV/3R1WiP0f3WNDxrpRKsy5uzaXrwpS0JgnOqIlTNfsoirGTYniSSpNpsZd4FuIkNCu7NJCqEPuXzrug90SMJt5HlZmHWWNQtuKM3Y+dSUqoKZUEaVpTPPnHTxeRjhg/ZWzLLxp5My4ukKmIYsEw8qvsNMiREQmIXPiGdty3aurVaV3qnYtmWw7NFoioXCvEBaeIY1eadmpmWVVw+W2ZObO1xdr6rP1mlOFVS1E8arCczJnLm9LqFCln3RROK0SK5sULrIgkP4RknNTVrs1vG4asXXMLouScUefw9EXdDQgB1hNfJLi+8kTG0zl6qYLhtSwR4itEeXswME5OkU3QHwTCFcfaBVVoHXNnf1e9ziWcBlxUFhassusjMXKPrbnFHhVNSIv4ovOLeFqw+4D1VRmWnScZZQrTJguIiIey0aZ7l8ofOn3RWQ9BU5cWwdY/1AltC2hNEKiW9aqlCr9FI60fHx4a1bfsCXlTnSb0vRlPSXA1B00bGi3XIKabgt6v1S+EQYRLTT3qnFOwhJsQXvlw/Df7oe8Cx5l9xmXntkbpDaD9QJt0+ERK3hLl8N12ZRJ2UGYNhhravNlaqoljbZkSNiAkXYp3KSJzp1YVj8eaybXQ/LngoaOcAwWbVkFMdqxtSHZLcbdgkQFyW6u/s3L4RTlcOlZp+ZZMTk3mn3W2nFuFvSSiA25JnaPjuh4wvGgk5Ym3RLbMC64Q6RuuNSSilS7iTdCDiOLekqrC5LMv7a+ttr4ipDbb2lbTzryjbBym2mjlNLtHLE6orZNaTEib2ndMeIS/WK2LdDDmlNwCAHSMCaBNLbmm4+WklXOu5c+2sXukEw5ZLz2yTYzzVrof91ugkXs+Hkq88qOEdJUllSy9WS4m142+8TZdZPD8N6hkipRKwznhnyj2KU3Kmwqg4KgY8SKNpQY6LurrH6UPHSs5RWEWeZdscG5hwQ9YJkOm0u1dOnnTdCTgDBsOqB1FbStrbcQZ6ra+yXvRU3xy82PitdHqoefHycDjJVKhvl5hxho3RFVtH6x28IlAfEekdy1EFR8h1Xf7YeyNV1U+Ec9ItoTAWHS10SyUdWkh6vhARlWxSiCSmPIUInNXzY7H4vFD4+Xs8v5bblXosNv8ANVyLlH3pa2qvInNmPtHxFFPZbRTJ4wZBsf5aKLr9/V0jw186+EVfSM0uSwGxKxK8PtF7XF9sdWUlVmWMLZL0Rmj/AIswTZVRuZIj7osXKJ6urkX4RskxMsnk3cp/SJv6RFlGafJ+2npim3QLmnbqXiFmnTpX5q5xoimvbXzUv7o8n5GXlLo6qx8dWdonej4s0pXiEvyxyJRcCVVEuco0nVrxldTqj42wiMW+gm6PzRhGJvyD60Um9YtvBTeImiqKovNFTLsjfU6RK40xYZoqcSo4wx1aWkrv9Iz75TcIbmDcmWG2mwY0uPohCUy4RWgIpWi/pRaqlKM0lh080DJSRK0r7bYlUBIb7bSPVw9sHmi019jMDW76D/SbEZmYkTbkK+mbISAVJraJuuK4dN1K0pz7Im+TXomGFyYtmSG+6W1epw7RRtsTwRLc+ea84KYBIIxqMlddc/3C4nDHiP2a+W61OyLRy2olZdUutYVwkPzbqXJDFBxQick3oLACD2xIlIDNkqrSrqedtn4wQl2+da/ei0AWkSOojz7Y6SLIZJ0wlUcfk7ABPYsAbgEriuz3RF0imrmlQCU1Ed2kmx+bmnLw5QvTr2oD27rq8IqqHp1d4lT35RcNwNmuv0hOqKuF/wDzJUuzjG53Y/j0Kjibhoum66JX6EKJ3Y7dRK7qfv7sStNgeRrb492MTbsfRTaaRVSsN+EozskGxFW0szMRD2bd3Pxhel1yUa5fRhlwt6xujKKukrqWCX0rhW7/ADDcXZU+jh1negqnq7R3GUAHmXLs6Dq7LYOjMLVeNF4c7bvpbo8YEHiVHKIDZbMloJE67b/LHvUuGq9uXbR+PFLLKoi5ZFjjbEvpNIK42BitygttEEs7qafFa7kgr8mOLlKK8PMkQrF0/j7oLdKcAI2/VATStjqvVqT2gCOm4iWglzzTmueeSRPkbbz5o+D6qu22yOidRIqWkla14d3Z2btmbxuOPjewfGzpz5SVJmznj52oSGYGbeTalKADh8rRNUPspATBhOZWYfFy1HX9XjYy2JonhUS+2EdvpPiCMARPWsmYtIdnEOdxbuVFg/0Gc/07qCoBaVyBtLisMrtQrzTn784zwxySb+h+ecXpDE7JuDwO3/TM/pWiUDpqSfKiX7JRLaEdp3WANw8Rd62C7Q2pqXLz/wA6YU+kuMP7XZSxs1dJmXFKXHfkRXW8Oohz9hY0+HBzyXXRlyOlRdd6MzBYUzMK9UNu/mm0F8miNREBGupLhTnurDb0JweYKRYvYR1HBczIxvJq5RG7V2CPKFXE+kz4AxJqDTrUm05wFbf6u24uLPi5c4f+juMTDElKirCNVYEl2m1ChFqLiTtKOpklkUa1dmehDwTo4YrYTTKNtzbY1v1CFxiX0qW/CC+F4c56W6YPpsiJp61FIRG4m3DEirpXSVcu33hWOkbz14CsvUplsTQVG8gMi0jcXzc05r8fH8RmWXpa15GUmZdvahYPq3eE96LqoW+NLjJ/8AYe6TTxmJiSlsGlGjzes39VpN5pSlbeaKtcoDYbhtziEFXnWnGiscD0ZwQtIStEq3aRHnnTnFB/FpgZR1h95DR5wniRWxtFq4dVoimakBZZcOS9tiWxMGUXZklCHmpcBCBCI14eMecZpvJj66NGOEJKn2NOKO+mys0wwiXtbObBaaNQ2mA97T9sZ3KyrDcwCzp0aG65bLm77VtEhLiHt+3KNw6Kz8u82y3s2wc2SOW2jlqUafdy8KQt9MeiLLZoYB6o7su6fd/T/EZXmTTVARg1JWAX+lEkEurEtKsPNu6zZRhnYOGI8RCIolaiO/wgX0dlg9HYKZAEeacJwFRBJwQLqkVdScWWapl2QYYwpAT+Un1x/KqRArSGtAGnglsc9ye0joRqiWdl9s0YKWi0SKi2kNpXCVpdkJQ4RvvMzTaXZLZd7JFDHiM0jIq1zLj/AOMBwfCvj2J/d3Y9F+N8dwx79nJzzcpOiAJVAXK2l3AiWWn3iuVbi98STMhV5GW1Srzdoqt1l5d62veHlBJpGyyJKXc+sMAzmTSZetQ/9M29rRCLiDTb7S8v/Ma8zSg11YnFfKx06M4ImGC6cybKq4IjmBiDdvVEipcq6eXLnFiZ6XyjdbVU7ezT+ZfahYncYN5u8yVPVCVKWjw6oz+am7yVUXKPMywpP9uztxUKTZrOB9OwOeaFUQGiF61OL1tui773LesNgYl6YimKrXaEIp80U/qZfCPz1LI4RorKGpitw2oRFl1so3P5N5mVfYNyYv2wuXE2gkFh3DxbqkpbvJYbHjFaEZqu0d9JZFv+GzAPaUG14eHqzAkI/ARH3wztvAtgovC2P3hT9++K2N4SE6ljTiKzM+rIluEm7iS0hFUz4U86e9C2CdHdgwDUwaPmLYtkSCrQnQbblRF3r5wMuLabFKVRopEe0oo6A6q0K8g72rq8/H3JFJp4zUzrkJEIp9K2DmOMm2FzYqaaq+wnzez8IXJEFBu0t4iNy948oZHaBDOH4ipuWqgU2QuVt1EZEQkO/wBkfjBQJoCWlEr5j+sAcMW1xFrTSQ7ruIhL+2Cb71ErXLyt/NCJqmWELwLnT3xOKwKl5pynAh/TG4fnDnFsZgUyL1a9lwr/AFgbIYHiJg4WgzdDtIBA/u8XxiT0qwaCYn4bBofvdWOJkFMkqgVLTWgj90eL4RI4Bj1WkTtQBH6uXNI5Te3ZsSVA+lV3ZXRaSgpoT7SsKKwsKXDE7aWt+PVz0QlMaeDpXdSCEiyq52p8bbvqwNAlHIs/KLUua+CfV/SCg0mRotT823LCprYiiQiDZXesPu29brc4CzeO+iijEmXrbRbN8VutuK3ZsF2d4+staUTiD9LJg3HkAKqjQ2p88tRF8LU9yxQwuXJx0GhRTcc0tNonE+WkPdUvsjr4Hxjr2ZpRTdv0MADt6oiX3astV0AZjBibcOoFYg7TPIkBa6k8qRo/RnCJFujTs6Jm24Q7FtLCIh0kIk7bcPzULnRVhnn8Bk3FaMGBvbLVe46YmwXG2Qoopnp8qJ5Lu+GUl0Y8nkwi6M0w3o8pys4xdeGy9Ok1Qh1GI6rfNLU9ywKbeekiQi3WtFkvE3cusfaFaQy4cXoD80wiKrO0fEFtEhlgMlEQEqKQlqy3Vr7UMfTfDknZJs5YGQVgdpQGwC5oh1jpTemkqezARxbYTzVX0wWz0jRuSmXDW56WEbR/6gnwH7Q9vl4wGkMWkHXm3FG8m7WUE7G29q4NtyKi1pW4t6Z1XKLs0djKPMq21MtejCoIN4k7eAkQ5JaI3LTfuXdSE7FmX2nH0mWlaN4Hd/DcTiOJaQ8WYqiJX9IkJLHdexj3s0botiWHMzKueiNXCVouKZuuCJEoja24W+pZqmdF5wx4soG44N+j+YACFpjpQrSJRX4ZdlcoxtueQX7HEsvEjPJdLhjdp5pxdnNUpBGUxt80OydmEW4bQvMidtG0tREIjlTLfl8VY+byqzVJ4+Fr6Ckxg0szOYk0CqYNti4FHCIyMhZPq05kUFsTlw9IkJo7bJxx/wBpwtnMIJjdVbeMq2/FUhUMHDnJp/bGq3A211dbwoLfPqov3ImxzDUYKWZJ03XSmJlsAQisvJwRG0iXTW6qx2VerOexlk8NZVmdB5oaSzEkLVf9t0xeJwx7qqpJ8IqyGGrNECNil7bREKKlze1bMRC4e7oEV8FijNy4hMUpfaLIy6pqtaCVERIiHdXkm/mvVhh6FzINzMka5I469Lllp1laIlnpqtvw84HIrg3ZUXTQR+TjEpmZxQlcUlVGHSeRRttpQRG3q50/axpuPsbSXc7RTaJ9H/FYXuh2DtS8zPPNhbe5syJLtRXKRUHzLlDRihUYfVd2wd/IscmbTehzezM5lo87UT4DA1At6tP33YIvO5raqwIxeeUG61rqER+t+l0ZcMHPKl9sfKTUWKuPzq7R3t2lo0+dxfCKUs4jeRKpn3B1fWKJGlR51TcRVRsRyTrGQ28XV60TkN+kF2SdUBQQP63W90eujpUjmdHYTaVz39ndgxhiB6Q005mj7ssRpw6HBRn+0oX2JVGySvet/wCP22x90qnVBoCbVUcNwBBUuEh2ZKVyeKLbCfK/8nZePU1Rtbi4fOCss8MsbBtN1FBRoBuNwdJDS1dFN8Y4ziWDtths5JtVIhzNNrbd2qVa0hW/jswQC2KmrlSGtRK5DJStQUTfUl5+7sIYV0QmX/5ihLhTnqMvIRjz2NxV8/8ARvbT6NG6OS8uT6mwkuDSWsmLTFpXFW3UNtV3ad+/ONMlAUFDYgB7JrQSOWi6BZiVyiqlxdtNSrVV3A8BlJaVkmGG0vAG3GzUk9YRkS3kVOsvb4eFIkw9kVmKgqkGxcZdBLW3B1EQkI5Daue7mvlSJUDNuX+BnJpwxS4GgNbkVUVbgHq2rTen9IIBu31hVwWTNt5HXgQT9ClGyWolbMgT208aFeOfspDGjnNP/rFi6LMYh076UzMjiMxKsbHZBs3MwIiEyAXLCW72uzmkbYJVjB/ltkvR8TYfyQZxga+062VpfdJuCxumMxJctgqY+UB8pnD3kDY7Mpll1BMiB1pzZd7dS37I1D+PoAor7W1Z0+sC4Sb+cI/j9kYxJYML0xJoq3ADW2fFFuIXCcMhCg5jUdkufasPeHYwBvm0BKist3GipxBcg6RLi4hhjimDOlJ0aJhjzLw7VgtqBc0W8fm87YtqQp/MFfrKv4xkWHYnsXldk/UqTjgmyupty0lHh634+/ONXwiYWZbadEKbRkD59bNR09i5QiUeJRhpryrXw7vzSj0QupnT639qR3s0GO3KdqUHhjz9/Z0DggAaZr5on/JY5Ff3144RfKJAdQFReV0SwqOaeK/WiZolyQfhxfmrFh+dZXOxT81tHh7opAqdnlAapWlC0tiAlw9YiraPbl5UjR48YvIr6vZU0+DrsJTzmCJX0wHZeZLV6h4nyM7es04i2184VsaAHzaKTGa2B6QvaFuYcMStK1AIrqLlAqRZMnwFCzdcEVNEO7UuZZp5+Maa46LZIDAogaRaRdVrVojxLmVUASXtWqx1vI8mENx19E8D8dk8h03pdgvD5CaaEDCSbNXCETUiF3YNpwjs0XUiJ+C1pD9g2IobDaiCHxCSK29yJR3p2WxQwSS9KJaCqog5reVq+zb7l+Ec4o+LFRba2p80X2e8SwrJ5ubIk26R0F+J8eM3CKuX9E9+SYdN70UVF0J2swqnpFwXhHaNiQ3WqhFdUtNFy3VYMJQzLYsoitNERVK0dB6QEbuy0VyTktYlkknCW7YtsNrqoIiBEV1dS/GsTA460qOk2BvVJrZiojcHW1KnYI8uVICOd3u6Gz/HxjF8UnJr/hQxyQcCpiKBwkNt9421tIbkS2mmq/qsCpzHHCYtqZvu7Sh1FrZugYEDdwrztdHxqiJ4tOKTrVlHRscUblC0DtuK0biTTnqy8fCEV5VUlJka3aXQqA6LdLmfZuVM+VO2NsZRkrPP5MUoOn2Cp4GplRcm9t6Q8LhoSWgpEhqOzKqLUkUS7OJEzpBbodivo47FhmYoT201KJ3O2oNokIpdwxbSWYHMRQ1buK8FK0XctVwoo3aeSeceMzDji0CtNVyVN0tJXahJUH7saPkhr7EKDCaKb3pLosvAbtpUNmwCPaNlcJFxUEOzevjEmI4lXYG+lXWmnGWmxC0mzMycNwruHiH4DvXJPpaYeNDWhojfEiKICWrSJCgpq7fBFTsiQJNXiMtiF5XWItxGQFQrytpcN13LyjVHJGSX8AaaByOv7JW9mtZkpR43CUbBtlWRMfaJCAskgvh0kklYhrtbbXPVrYV+zIbLiReRF9dN1IkmMDceZSrqVYFzfpDZXabbeLrV86VWlYlw/BHDVbzBpPWCKoBcBV4e6NSp2fDJWTOkqLUbHiR6UMZNMAdEHSRWjcXu3846n8TfcE0RFJFuFURBLR83rQnSOBvsOIW1BUEiyS/Vb1SGkGlfmN1LEIdNF4vnfp4RzWx1IGo1UrQljVfMrvpXdX3e+ELp/iKg+DNADYtiRIKiWsi6xCvFprTxjT5mZBlg1fJ11BG4kELRIO7p4uHP7Uj8/Y7iKzL7zpIgK45dYnA2HVAfJKJ7od4kanyfouTtUGcOd9WnaRbuH2eLuxMTh1VAeRFHiRGb7fnEVfxrAIZ/SgAtNO9LRL6yxzLzyhkFK9q8I/rHaXkRSEfFYVmMSW4Ecpe2REdOEtOkhu5al+EWmHgeW06UbEiqvtCngvtcucDBxJLfWavNLombnUzSiJ9Eb+HTqheaXyQcU+woQUWnQfwBmVlnDK1EvIqqpcWq627qp+/GGVvEAuCpJZtNK923qj+kJUtjKGiCiBeI77B63EJQxzQMuEBMJstpLtuECXEA3abbhrb+EcLNgniqTdm2DjK6VDU0bbiXX+9TIPy0/GnnACemTB4DlldA/wDaNE0uGOrVcuoVS7km+uSwEm8IfFFt0J88Qu+jWBcvh7jZKQFRRISFFuIHDErtRDC3kk19BxguzaMH6QpMihLUFLiSy60/rbvdBqWxFmuTw17NP9qrGPymKTQMmhtaC1ZXCAuiNouXU09i9sM+B9NQJLXiQFHkSCP1SGl3lSGRnaFzxfRp7LmeS5QjfLNgZzuHITWxQ5V4XlM7BoxmJLtC4US4SXtt5wfksYlxbQ1dCwrbVRepHeNTUnNSTwOPBsZlpxm9LT4kUeFeJU7PCC5IzuLT0fn3o/KCyvpM8nqXREmzG8iHdaREPCNN0Oh9H8QxNQ2Kuy0sP+84pBdq7vW/xvgnKYjJ4eIN4e0TxppR1bNPzU6qQSxnH1GXN+caP1Y3WAVoufW4YGWf1EP4ndyPsB6MYZJ0vVZx8SuIi4BPvWj/AFrDc0+pJpqI8kHJIzj5PsbPENs6QINz+zaZHqtCKdbrcWcaOF7SCgiirTNa0FPZTyjNOcr2E4JdGIl5r8I5Wu4d8WjBPuxCTaRyWzSiG9RyJGl87Y5SZtyoHwuiVZdO2nmkR7HxiWWRrMGXDl5aYqkN1c84uKzHbbdM6VhkJBIqSMmjJXWpf1eIbbv1T7Fi67OLtEUkpkQ5LdqXrR4QZ1plduSJlQzSlE7taD+am+Lm23Z6DwPNxQio1Q09CsbRr1JqGycQsqahO5FuWi57vw99+bcbbeURtcOxs1VLBQiVKraicv1WFvCkVvcIKvgA6dXhv+EdYiqqV3MurwnpjRHK4wSYMoYp+Q5xdWt/5CGJ49ZuFUyLNdP75QozGIOmdykqKhFREuu7cvjBYmEJEU0u99x/djhZECzt+Nw3QuU5SNMc+HDF0tg8zccqRkaqYjUU7ntF1q791EiApO1UIaJq6q2l91YYBlUNdypd9Lh+d/Tsj1+SUFT61aj+WvDwxrxzajR5bypc8rl9i4bS8Kbvrf01QTwwTBUUlVeqOshEfqxYodaaF8dIH92LcqyBIqlv8V0/dT+sacckv6ZJFqjeSejpVwbRK4R+kRXJaPFHjKKdUZREXhIEQruHV23VtrktPjBWQblDT16Lf87QW7s/ecXRmQRF2AGa9WiaBD6PD5RpefQniDZNt6ijbVCHhW/VdS78sEZVpxlK7JOHcpv3feT7KV7IpzOJudtLexfu3DETeIzJZJetv0j1U4ruUZ3MLiGjBFT+Si9XcRAID85E8+yK8/MWItQALbRFVDQWnT+UvhHbU4ZpoVdN24buH4Xf1iB2bcGm0MwuIf8AbIdGf28OcS0ShMx50zqlES4daCAj+UtVYQJ7D0ElvF1Fu1U+9pJOL3xrGKreq3vOptOaqY7MLuG0eLL8IW57DQN21skeu4SVNl+ZdPxi1lcdIujORbTaKNpqPKiWll1recE5fBlUdowoP57lXZEPmhQfDC3GSB4FsUS0GikHslaWXepvrEbmFKFRUaai9rrKP0s7oZDya7C4goMDqJK46yr5cLImJHd1R05RVOSdN0wYS9RUROiKQjaNt131vgsNEpgikOVFuK0QRQ2hH7I1u+yLreCIeYVIyJsSRRs15cRcI5l2+dIOXla0i1ATWsFcRdZ7JeVAv6yj26YccFngkRNDUpt61sRXUDYgNC6yXIX6JFfEZVpirYpc7zO8SFv2Rpz8aqnZA9co52byZdHpfxv4eM488vT9BOaxyaeKtwB4IA8PduKJgnFNFQt/amm6Aox23NkC1hEcv2egj4mCCpRQQOZe6hVQt6UjgJ94FyEfqDFZJ1brsqc0TTBNxEVLm1HhIuqJDDIyvoXPBhW3FUePdJ55plxGxlxvG3U2m0QfZqv9ItyPTWYnGlZmGmSfJf5iN2laI9YetCPi0+KEqXEpV574tdHcUVtFMt9bR+bDscZS70ee8vJ4kMlpJ16DE30rcklRthsKjvUwu+0oY8Nxf+PSxyYMmM08oi6+iD6O1LdZwvGmSIvNUhcnsXZmRtMAXzSCGBdMG8OaVlhloLiuI003H7XepDnj49HIzeR8jf60aRh2FYd0Xk3FDaOE4RFqUTmH3eq2FqJ2JuTxWC2FSxPNi9OiO2dS4gTha7Gx8ETnzzWMUlekCHOg++Ru7O6xFW4W7u6PVjXMC6UsTDeRoBDvRcoTkUn60ZkqX9Mxrl+yjgqxJZHSDHENZEDBlW1Kx6AKOcTmleqnD++ceBRc7Rp9L9Ysh0DqFkSJ9UbPnaeGOzbsoQKqfNK36pRIzRUJCsEe8jYkXu3fbErA5ogEuW4uH8INMhRbZu3LTz0xI3L57/ekSk3Ulpnb/wAc48BInJhJ0dbAxSo1oPNE/wCMcq6qpuWviX+IvSsxoJEWldCpbWo/OqnhEbrOdNJeVw/jDHLQUJNO0yJEVOBPndyOmh72+JUap9W6Ogbzikw5ZZP2SIXYldPsxy4JnmaxaQKZV097/wCsdEoonMq71XT91F/rGmMvsxyIGcOqNxElPBCu+buj1wGQppVV03JeX5qf0j5XlJKKq0HdqKOAS5U5Z0u8e3dWC+T6A4l2UmWUrayNfaO7+nbFtlhxwbnDRpnsG3V7IiKpFKUBVJLU0iQmRItpWeFecSzxlci6mxTUmdxfisMU/bBcSUZNs++iauK0T9kbRShV84nR1sG1QCQEMbSCgvuF3eWmKDTrS5qKkdvPhiZx0xS1sUaUrcxtv+tv+2L51sqiEENsrjqiOXFVdJFd8PsiZ98yWt7tBG5oBXR3eHq/ulaRVMiHVeqlWletXtrHYtK8iHpqhZZW184BZfRfErzhAbms1VLitQtdvsl3i09mcV3sPYcvMFsTaWiCkI26eLenPlSPZhtSVaoKeWmJCwstmrlUyVEROaqvLspFKbbCqgdNYaFclBU6tFItHtF3o5l5MN91OK1eG08tQlVNUGGsHcOiZCSbxVf7krHh9H3B3qieS/4gk32QptWf9VmpXXE4HrL+9dRefjlnEWJOy0sy6QGBm5pBtNqQX94rl5dqpyi6/ghAOokVSSvu7IT+kZWOACbrbouWRpG38fhjlzRT6B1fjHixyC849UoxydnvIUlSPCKPqpHCx4qxVAuR0UdOJcNPZtiFTirNTVEhkE70Zs+aEYtyBeIMKhKSrWq713xWSZUUtRco6nJiqxRjqY062eD8rJBZHw6LXpJ9qxyT6rzWK9Y+g6MzysvS06oLnBNjHjHdVPfC+kTCsC1QUJcuz//Z";

            }

                 return (
                    <div class = "profile">
                        <img src={eachOrg.image_medium} style={{width:'311px',height:'200px'}} alt = 'image' ></img>
                        <h1>{eachOrg.name} </h1>
                        <p>City: {eachOrg.city} </p>
                        <p>State: {eachOrg.state} </p>
                        <p>Status: {eachOrg.status} </p>
                        <p>Number of Donations: {eachOrg.number_of_donations} </p>

                        <h2><Link to={here}>Read More</Link></h2>

                    </div>
                )

                })
           );

        return (
            <div>
            <div class = "description">
            <h1>  Organizations that can help.  </h1>
            </div>
            <div className = "search">
                <form className = "form">
                    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
                    <input type="text" placeholder="Search.." name="search" value={this.state.input} onChange={this.handleInput.bind(this)}/>
                    <button type="button" onClick={this.handleSubmit.bind(this)}>Search</button>
                </form>
                <p> </p>
            </div>

            <center><div class="sortandfilter">
             <div class="filter">
            <p> Filter by Name: </p>
            <center><select  onChange = {this.selectOrgName.bind(this)} > <option> Select... </option> {name_options} </select></center>


            <p> Filter by City: </p>
            <center><select onChange = {this.selectOrgCity.bind(this)}> <option> Select... </option> {city_options} </select></center>

            <p> Filter by Status: </p>
            <center>
            <select onChange = {this.selectOrgStatus.bind(this)}> 
                <option> Select... </option> 
                <option value="active">Active </option>
                <option value="inactive">Inactive </option>
            </select>
            </center>


            <p> Filter by State: </p>
            <center><select onChange = {this.selectOrgState.bind(this)}> <option> Select... </option> {state_options} </select></center>

            <p> Filter by Donations: </p>
            <center>
            <select onChange = {this.selectOrgDonations.bind(this)}> 
                <option> Select... </option>
                <option value="0">0+ Donations </option>
                <option value="10">10+ Donations </option>
                <option value="50">50+ Donations </option>
                <option value="100">100+ Donations </option> 
                <option value="500">500+ Donations </option>
                <option value="1000">1000+ Donations </option> 
                <option value="5000">5000+ Donations </option>
                <option value="15000">15000+ Donations </option> 
                <option value="30000">30000+ Donations </option> 
            </select>
            </center>

            </div>
            <div class="sort">
            <h1> Sort by: </h1>
            <select onChange = {this.sortBy.bind(this)}>
                    <option> Select... </option>
                    <option value="name">Name</option>
                    <option value="city">City </option>
                    <option value="status">Status </option>
                    <option value="state">State</option>
                    <option value="number_of_donations">Donations</option>

            </select>
            </div>
                        </div></center>



            <center>
            <div class= "all_dis">
                {!this.state.clicked_search && (this.state.clicked_filterName || this.state.clicked_filterCity ||
                    this.state.clicked_filterState || this.state.clicked_filterStatus || this.state.clicked_filterDonations || this.state.clicked_sortBy) ?
                    <h1> {finalResult} <Pagination items={this.state.filterRes} pageClick={this.pageClick.bind(this)}> </Pagination></h1> : null}

                {!this.state.clicked_search && !((this.state.clicked_filterName || this.state.clicked_sortBy||this.state.clicked_filterCity ||
                    this.state.clicked_filterStatus || this.state.clicked_filterState || this.state.clicked_filterDonations)) ?
                    <h1> {org} <Pagination items={this.state.organizations} pageClick={this.pageClick.bind(this)}> </Pagination></h1> :null}

                {this.state.clicked_search && this.state.organizationIds.length != 0 ? <h1> {search_org}
                    <Pagination items={this.state.searchRes} pageClick={this.pageClick.bind(this)}> </Pagination> </h1> : null}
                {this.state.clicked_search && this.state.organizationIds.length == 0 ? <h1> OOPS NO RESULTS :( </h1> : null}
            </div>
            </center>
            </div>


            );

    }

}
export default Organizations;
