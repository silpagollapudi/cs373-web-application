import React, { Component } from 'react';
import {Jumbotron,Button} from 'react-bootstrap';
import silpa from "../../images/silpa.png";
import danyaal from "../../images/danyaal.png";
import sanjna from "../../images/sanjna.png";
import eduardo from "../../images/eduardo.JPG";
import brandon from "../../images/brandon.jpeg";
import './about.css';
import { Card, CardImg, CardText, CardBody,
CardTitle, CardSubtitle }  from 'reactstrap';



export class About extends Component {

    state = {
     // Contains all relevant information about each developer
     developers :[
        {name : "Silpa Gollapudi",
         bio:
         <div>
         <p>
         <img src={silpa} style={{width:'100%',height:'100%'}} alt="Avatar"></img>
         </p>
         <h3>
         Senior at UT responsible for frontend. I love dancing and watching football! #GoCowboys
         </h3>
         </div>,
         commits : 0,
         issues : 3,
         unit_test : 1,
    },
        {name : "Danyaal Ali",
         bio:
         <div>
         <p>
         <img src={danyaal} style={{width:'100%',height:'100%'}} alt="Avatar"></img>
         </p>
         <h3>
         Junior at UT responsible for backend/frontend. I love playing sports and exploring new places!!
         </h3>
         </div>,
         commits : 0,
         issues : 0,
         unit_test : 1,
    },
        {name : "Sanjna Sunil",
        bio:
        <div>
        <p>
        <img src={sanjna} style={{width:'100%',height:'100%'}} alt="Avatar"></img>
        </p>
        <h3>Senior at UT responsible for frontend. I love traveling and trying new cuisines!</h3>
        </div>,
        commits : 0,
        issues : 4,
        unit_test : 1,
    },
        {name : "Eduardo Pineda",
        bio:
        <div>
        <p>
         <img src={eduardo} style={{width:'100%',height:'100%'}}alt="Avatar"></img>
        </p>
        <h3>Senior at UT responsible for backend. I love playing video games and recently have been into running!!</h3>
        </div>,
        commits : 24,
        issues : 0,
        unit_test : 4,
    },
        {name : "Brandon Manibo",
        bio:
        <div>
        <p>
         <img src={brandon} style={{width:'100%',height:'100%'}} alt="Avatar"></img>
        </p>
        <h3>Senior at UT responsible for backend. I love playing volleyball and eating!!</h3>
        </div>,
        commits : 23,
        issues : 0,
        unit_test : 5,
    }],

     totalTests:40,
     totalIssues :20,
     totalCommits:121,
     // Array to keep track of all the tools used in this project
     tools:[
        {name:'Amazon Web Services (AWS)',desc: 'Used to host our website',link:""},
        {name:"GitLab",desc: "Used to house code repository and task management",link:"" },
        {name:"React-Bootstrap",desc: 'Used as a style framework',link:""},
        {name:"mySQL",desc: "Used as a database",link:""},
        {name: "Postman",desc: "Used for REST API testing and documentation" ,link:""},
        {name:"Namecheap",desc: "Used to claim a domain name",link:""},
        {name:"Google Docs",desc: "Used to collaborate for technical report",link:""},
        {name:"React.JS",desc: "Used as a framework for front end design",link:""},
        {name:"React-router",desc: "Used as a dynamic system to help front end design",link:""},
        {name: "Selenium", desc: "Used as a GUI to test the front-end", link:""},
        {name: "Docker", desc: "Used to create a docker image to run application in container", link:""},
        {name: "Flask", desc:"Used as a Web framework to work with API" , link:""},
        {name: "Mocha", desc: "Used to test frontend", link:""},
        {name: "React Google Maps Api", desc: "Used to see locations of cities using the google maps API", link: ""},
        {name: "React Highlight Words", desc: "Used to see highlight keywords to emulate a website search", link: ""},
        {name: "React Select", desc: "Used to display dropdown menus to be able to filter and sort attributes of models", link: ""},
        {name: "D3", desc: "Used to create our Visualizations using the data from our models", link: ""},
        {name: "react-twitter-embed", desc: "Used to display related Twitter Tweets to increase user engagement", link: ""},
     ]}


    componentDidMount()
    {
        // Getting total number of commits for each developer using gitlab 
        fetch('https://gitlab.com/api/v4/projects/8559393/repository/contributors?sort=desc')
            .then(response => response.json())
            .then(data =>  {
                    for (var i of data){
                        this.state.totalCommits += i.commits;
                    if(i.name === "Silpa Gollapudi") {
                        this.state.developers[0].commits += i.commits;
                    }
                    if(i.name === "Danyaal  Ali") {
                        this.state.developers[1].commits  += i.commits;
                    }
                    if(i.name === "sanjnasunil") {
                        this.state.developers[2].commits  += i.commits;
                    }
                    if(i.name === "ep23934") {
                        this.state.developers[3].commits  += i.commits;
                    }
                    if(i.name === "Brandon Manibo") {
                        this.state.developers[4].commits  += i.commits;
                    }
                }

                this.setState(
                    {ready: true}
                );
            })
            .catch(err => console.error(this.props.url, err.toString()));

        // Getting total number of issues for each developer using gitlab 
        fetch('https://gitlab.com/api/v4/projects/8559393/issues')
            .then(response => response.json())
            .then(data => {
                for(var i of data){
                    this.state.totalIssues++;
                    if(i.author.name === 'Silpa Gollapudi') {
                        this.state.developers[0].issues += 1;
                    }
                    if(i.author.name === 'Danyaal Ali' ) {
                        this.state.developers[1].issues  += 1;
                    }
                    if(i.author.name === "sanjna") {
                        this.state.developers[2].issues  += 1;
                    }
                    if(i.author.name === 'Eduardo Pineda') {
                        this.state.developers[3].issues  += 1;
                    }
                    if(i.author.name === "Brandon Manibo") {
                        this.state.developers[4].issues  += 1;
                    }

                }
                this.setState(
                    {ready: true}
                );
            })
            .catch(err => console.error(this.props.url, err.toString()));

    }

render(){

// Go through each developer in the developer array and display stats
const devps= (
this.state.developers.map((item,index) =>{

return (
    <div class="profile">
      <h4><b>{item.name}</b></h4>
      <p>{item.bio}</p>
      <p> <b> Commits: </b> {item.commits}  <b>Issues:</b> {item.issues}  <b>Unit Tests: </b>8</p>
    </div>
)})

 );

// Go through each tool in the tools array and info
const tools =
this.state.tools.map((tool,index) =>{

return (

        <div >
          <Card>
          <div className = "tool_descrip">
            <h2>{tool.name}</h2>
                {tool.desc}
            </div>
          </Card>
        </div>



)});


    return (
      <div>
   <body>
      <div className="mission">
         <div>
            <Jumbotron  id = 'jumbo'>
               <h1> Our Mission: </h1>
               <h2>
                  Provide information and resources on Natural Disasters 
               </h2>
               <h2> Total commits : {this.state.totalCommits} </h2>
               <h2> Total issues : {this.state.totalIssues} </h2>
               <h2> Total unit tests : {this.state.totalTests} </h2>
               <p>
               <h1><a href="https://gitlab.com/silpagollapudi/cs373-web-application.git">GitLab Repository</a></h1>
               <h1><a href="https://documenter.getpostman.com/view/5481959/RzZAixhn">Postman API</a></h1>
               <h1> <a href= "http://elasticbeanstalk-us-east-2-800391164604.s3-website.us-east-2.amazonaws.com/"> Visualizations </a> </h1>
               </p>
            </Jumbotron>
            <h2> Our team : </h2>
         </div>
         <div class= "mama">
            {devps}
         </div>
         <h1>Tools used to create this Website</h1>
         {tools}
         <h2> Data Sources </h2>
         <div class="sources">
            <h3> <a href="https://api.sigimera.org/">Sigimera.org</a> </h3>
            <h3> <a href="https://www.yelp.com/fusion">Yelp.com</a> </h3>
            <h3> <a href="https://www.globalgiving.org/api/">Globalgiving.com</a> </h3>
         </div>
      </div>
   </body>
</div>
    );
  }}

export default About;
