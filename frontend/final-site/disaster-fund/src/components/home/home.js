import React, {Component} from "react"
import ReactDom from 'react-dom'
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';
import one from "../../images/4.jpg";
import two from "../../images/2.jpg";
import three from "../../images/3.jpg";
import four from "../../images/1.jpg";
import five from "../../images/5.jpeg";
import six from "../../images/6.jpg";
import './home.css'
import Typing from 'react-typing-animation'

// this class is the for the main splash page and the slideshow
class Home extends Component {



		render() {
	    return (
	      <center>
				<div className="splash">
					<link href="https://fonts.googleapis.com/css?family=Fira+Sans" rel="stylesheet"/>
					<Typing speed={200}>
					<h1> You can help. </h1>
					</Typing>

					<Carousel>
	                <div>
	                    <img src={one} style={{width:'100%',height:'100%'}}></img>
	                </div>
	                <div>
	                    <img src={two} style={{width:'100%',height:'100%'}} alt="Avatar"></img>
	                </div>
	                <div>
	                    <img src={three} style={{width:'100%',height:'100%'}} alt="Avatar"></img>
	                </div>
									<div>
	                    <img src={four} style={{width:'100%',height:'100%'}} alt="Avatar"></img>
	                </div>
									<div>
	                    <img src={five} style={{width:'100%',height:'100%'}} alt="Avatar"></img>
	                </div>
									<div>
	                    <img src={six} style={{width:'100%',height:'100%'}} alt="Avatar"></img>
	                </div>
	            </Carousel>

				</div>
				</center>
    );
  }
}


export default Home;
