from sys import platform
import unittest
from unittest import TestCase
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

import requests

# options = webdriver.ChromeOptions()
# options.add_argument('--headless')

if platform == "linux" or platform == "linux2":
    driver = webdriver.Chrome('./chrome_drivers/chromedriver_linux')
elif platform == "darwin": # macOS
    driver=webdriver.Chrome('./chrome_drivers/chromedriver_macosx')
elif platform == "win32":
    driver=webdriver.Chrome('./chrome_drivers/chromedriver.exe')


driver.set_page_load_timeout(30)
driver.get("http://disasterfund.me/")
driver.maximize_window()
driver.implicitly_wait(5)

class MyUnitTests (TestCase):

    def test1(self):
        """
        Check if the components in the navbar are correct
        """
        try:
            driver.find_element_by_link_text("DistasterFund").click()
            driver.find_element_by_link_text("About").click()
            driver.find_element_by_link_text("Disasters").click()
            driver.find_element_by_link_text("Shelters").click()
            driver.find_element_by_link_text("Organization").click()
            pass
        except NoSuchElementException:
            self.fail("The element does not exist")

    def test2(self):
        """
        Check if the Disasters Read Me button is clickable
        """
        driver.get("http://disasterfund.me/disasters")
        try:
            driver.find_element_by_link_text('Read More').click()
            pass
        except NoSuchElementException:
            self.fail("The element is not clickable")

    def test3(self):
    """
    Check if the Disasterstitle exists
    """
    driver.get("http://disasterfund.me/disasters")
    try:
        driver.find_element_by_link_text('Natural Disasters Affect Everyone.')
        pass
    except NoSuchElementException:
        self.fail("The element is not there")


      def test4(self):
      """
      Check if the Disasters title exists
      """
      driver.get("http://disasterfund.me/disasters")
      try:
          driver.find_element_by_link_text('Search').click()
          pass
      except NoSuchElementException:
          self.fail("The element is not clickable")


        def test5(self):
        """
        Check if About page title exists
        """
      driver.get("http://disasterfund.me/About")
      about_link = driver.find_element_by_id("Our Mission")
      about_link.click()
      self.assertEqual(driver.url + "about", driver.current_url)
      time.sleep(1)


      def test6(self):
      """
      Check if Gitlab repo is clickable
      """
      driver.get("http://disasterfund.me/About")
      try:
          driver.find_element_by_link_text('Gitlab Repository').click()
          pass
      except NoSuchElementException:
          self.fail("The element is not clickable")

    def test7(self):
    """
    Check if postman api is clickable
    """
    driver.get("http://disasterfund.me/About")
    try:
        driver.find_element_by_link_text('Postman API').click()
        pass
    except NoSuchElementException:
        self.fail("The element is not clickable")


    @classmethod
    def tearDownClass(cls):
        driver.close()
        driver.quit()


if __name__ == "__main__":
    unittest.main()
