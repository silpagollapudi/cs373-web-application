import React from 'react';
import {about} from '../src/components/about/about.js';
import {configure, shallow} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';


var assert = require('assert');
var expect = require('expect');
configure({adapter:new Adapter});


describe('Array', function() {
  describe('#indexOf()', function() {
    it('should return -1 when the value is not present', function() {
      assert.equal([1,2,3].indexOf(4), -1);
    });
  });
  describe('#length', function() {
    it('should return 3 for this particular array', function() {
      assert.equal([1,2,3].length, 3);
    });
  });
});

describe('About', function() {
	describe('About Component', function() {
		const aboutComp = shallow(<About/>)

		it('Render about component', () => {
			expect(aboutComp).to.have.length(1)
		})
	})
})

describe('Test Navigation Bar', () => {

    before(function() {
      this.jsdom = require('jsdom-global')();
    })

    after(function() {
      this.jsdom();
    })

    const wrapper = shallow(<Navbar />);
    it('exists', () => {
      expect(wrapper.find('navbar').exists()).to.eql(true);
    });
});


describe("Test About", function() {

  before(function() {
    this.jsdom = require('jsdom-global')();
  })

  after(function() {
    this.jsdom();
  })

  const wrapper = shallow(<About />);
  it('Test about xist', () => {
      expect(wrapper.find('#profile').exists()).to.eql(true);
  });
  it('Test all tools/links exist', () => {
        expect(wrapper.find('#tools').exists()).to.eql(true);
  });
});
