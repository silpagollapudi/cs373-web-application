var member_emails = ["silpa.gollapudi@gmail.com", "danyaalali@Danyaals-MacBook-Pro.local", "sanjnasunil@yahoo.com",
    "e.pineda@utexas.edu", "brandon.manibo@gmail.com"];

var member_usernames = ["silpagollapudi", "Danyaalali", "sanjnasunil", "edibertusmax", "brandonmanibo"];

// Initialize all gitlab stats
for (let i = 0; i < member_emails.length; i++) {
    document.getElementById(member_emails[i] + "-commits").innerHTML = "Commits: 0";
    document.getElementById(member_usernames[i] + "-issues").innerHTML = "Issues: 0";
    document.getElementById(member_usernames[i] + "-unittests").innerHTML = "Unit Tests: 0";
}

function populateCommits(contributors) {
    for (let contributor of contributors) {
        let email = contributor["email"];
        if (member_emails.indexOf(email) > -1) {
            document.getElementById(email + "-commits").innerHTML = "Commits: " + contributor["commits"];
        }
    }
}

function populateIssues(issues) {
    var member_issues = {};

    for (var member of member_usernames) {
        member_issues[member] = {}
    }

    for (var issue of issues) {
        for (var assignee of issue["assignees"]) {
            var username = assignee["username"];
            if (!("issues" in member_issues[username])) {
                member_issues[username]["issues"] = 0;
            }
            member_issues[username]["issues"] += 1;
        }
    }

    for (var member in member_issues) {
        var assigned_issues = member_issues[member]["issues"];
        document.getElementById(member + "-" + "issues").innerHTML = "Issues: " + assigned_issues;
    }

}

// Get commits from Gitlab API
var requestCommits = new XMLHttpRequest();
requestCommits.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
        const contributors = JSON.parse(this.responseText);
        populateCommits(contributors);
    }
};

requestCommits.open("GET", "https://gitlab.com/api/v4/projects/8559393/repository/contributors?sort=desc");
requestCommits.send();

// Get issues from Gitlab API
var requestIssues = new XMLHttpRequest();
requestIssues.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
        const issues = JSON.parse(this.responseText);
        populateIssues(issues);
    }
};

requestIssues.open("GET", "https://gitlab.com/api/v4/projects/8559393/issues");
requestIssues.send();